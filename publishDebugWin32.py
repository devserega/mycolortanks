#!/usr/bin/python
# coding=utf-8
"""****************************************************************************
Copyright (c) 2019 (MyColorTanks automate project publish)
problem with cyrillic symbols in filenames
****************************************************************************"""

import os
import glob
import shutil
import sys
import errno
import zipfile

import time

makeZip = 0 #enable/disable ZIP archive make
dstDir = 'publish/Debug.win32'
srcDir = 'proj.win32/Debug.win32'

def copyFiles(srcDir_, mask_): 
	for file in glob.glob(srcDir_+mask_):
		print file                                                                                                                                        
		shutil.copy(file, dstDir)
		
def copyFolder(srcPath, dstPath, ignorePatterns=None):
	# Copies the files and folders from a specfied folder to the renamed folder
	try:
		# This copies directories only
		shutil.copytree(srcPath, dstPath, ignore=ignorePatterns)
	except OSError as e:
		# Here we check if the error was a result of the source not being a directory
		if e.errno == errno.ENOTDIR:
			shutil.copy(srcPath, dstPath)
		else:
			print(u'Copy unsuccessfull!!тест')

def copyFile(srcPath, dstPath):
	shutil.copy(srcPath, dstPath)

if not os.path.exists(dstDir):
	os.makedirs(dstDir)
	
copyFiles(srcDir, r'/*.dll')
copyFiles(srcDir, r'/*.exe')
#copyFolder('proj.win32/Debug.win32/snd', dstDir+'/snd', shutil.ignore_patterns('*.mp3', 'specificfile.file'))
copyFolder('proj.win32/Debug.win32/Resources', dstDir+'/Resources')
copyFiles('thirdpartylibs/win32', r'/*.dll')
#copyFile(srcDir+'/steam_appid.txt', dstDir+'/steam_appid.txt')

# create zip archive (not work with russion symbols in filename)
#if makeZip==1:
	#	archiveFileName = time.strftime("Nimormata_Debug_%Y_%m_%d_%H_%M_%S")
	#	print('Create archive %s.zip' % archiveFileName)	
	#	shutil.make_archive(archiveFileName, 'zip', dstDir)

	#zf = zipfile.ZipFile("myzipfile.zip", "w", zipfile.ZIP_DEFLATED, allowZip64=True)
	#for dirname, subdirs, files in os.walk("publish"):
	#	zf.write(dirname)
	#	for filename in files:	
	#		print filename
	#		if ".png" in filename:
	#			os.system("pause")  
	#		zf.write(os.path.join(dirname, filename))
	#zf.close()

os.system("pause")  



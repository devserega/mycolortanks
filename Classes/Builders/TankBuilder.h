#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _TANK_BUILDER_H_
#define _TANK_BUILDER_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "Models/TankModel.h"
//----------------------------------------------------------------------------------------------------
// Abstract Builder
class TankBuilder;
typedef std::shared_ptr<TankBuilder> TankBuilderRef;
class TankBuilder
{
	protected:
		TankModelRef mTankModel;
		cocos2d::Rect mBorders;

	public:
		TankBuilder() {}
		virtual ~TankBuilder() {}
		TankModelRef GetTankModel() { return mTankModel; }
		virtual void createNewTankModel();
		virtual void addPlatform() = 0;
		virtual void addCannon() = 0;
};
//----------------------------------------------------------------------------------------------------
#endif // _TANK_BUILDER_H_
#include "RedEnemyTankBuilder.h"
#include "Controllers/AI/RedTankAI.h"
#include "Models/Cannons/SingleCannonModel.h"
#include "Models/Cannons/DualCannonModel.h"
#include "Models/Cannons/CrossCannonModel.h"
#include "common.h"

using namespace cocos2d;

RedEnemyTankBuilder::RedEnemyTankBuilder() {
}

RedEnemyTankBuilder::~RedEnemyTankBuilder() {
}

void RedEnemyTankBuilder::createNewTankModel() {
	TankBuilder::createNewTankModel();
	mTankModel->setCurrArmor(10);
	mTankModel->setMaxArmor(10);
	mTankModel->setBounty(15);
	mTankModel->setTeamIndex(2);
}

void RedEnemyTankBuilder::addPlatform() {
	PlatformModelRef platformModel = std::make_shared<PlatformModel>();
	platformModel->setType(PlatformModel::Type::RED);
	platformModel->setRotationSpeed(200.0f);
	platformModel->setOwner(mTankModel);
	mTankModel->setPlatform(platformModel);
}

void RedEnemyTankBuilder::addCannon() {
	DualCannonModelRef cannonModel = std::make_shared<DualCannonModel>();
	cannonModel->setStartBulletSpeed(PLAYER_BULLET_SPEED);
	cannonModel->setReloadTime(0.5f);
	cannonModel->setOwner(mTankModel);
	mTankModel->setCannon(cannonModel);
}
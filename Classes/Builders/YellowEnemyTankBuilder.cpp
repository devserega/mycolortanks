#include "YellowEnemyTankBuilder.h"
#include "Controllers/AI/RedTankAI.h"
#include "Models/Cannons/SingleCannonModel.h"
#include "Models/Cannons/DualCannonModel.h"
#include "Models/Cannons/CrossCannonModel.h"
#include "common.h"

using namespace cocos2d;

YellowEnemyTankBuilder::YellowEnemyTankBuilder() {
}

YellowEnemyTankBuilder::~YellowEnemyTankBuilder() {
}

void YellowEnemyTankBuilder::createNewTankModel() {
	TankBuilder::createNewTankModel();
	mTankModel->setCurrArmor(8);
	mTankModel->setMaxArmor(8);
	mTankModel->setBounty(10);
	mTankModel->setTeamIndex(2);
}

void YellowEnemyTankBuilder::addPlatform() {
	PlatformModelRef platformModel = std::make_shared<PlatformModel>();
	platformModel->setType(PlatformModel::Type::YELLOW);
	platformModel->setRotationSpeed(200.0f);
	platformModel->setOwner(mTankModel);
	mTankModel->setPlatform(platformModel);
}

void YellowEnemyTankBuilder::addCannon() {
	DualCannonModelRef cannonModel = std::make_shared<DualCannonModel>();
	cannonModel->setStartBulletSpeed(PLAYER_BULLET_SPEED);
	cannonModel->setReloadTime(0.5f);
	cannonModel->setOwner(mTankModel);
	mTankModel->setCannon(cannonModel);
}
#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _PLAYER_TANK_BUILDER_H_
#define _PLAYER_TANK_BUILDER_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "TankBuilder.h"
//----------------------------------------------------------------------------------------------------
class PlayerTankBuilder
	: public TankBuilder
{
	public:
		PlayerTankBuilder();
		virtual ~PlayerTankBuilder();

		virtual void createNewTankModel() override;
		virtual void addPlatform();
		virtual void addCannon();
};
//----------------------------------------------------------------------------------------------------
#endif // _PLAYER_TANK_BUILDER_H_
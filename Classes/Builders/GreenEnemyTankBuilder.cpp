#include "GreenEnemyTankBuilder.h"
#include "Controllers/AI/RedTankAI.h"
#include "Models/Cannons/SingleCannonModel.h"
#include "Models/Cannons/DualCannonModel.h"
#include "Models/Cannons/CrossCannonModel.h"
#include "common.h"

using namespace cocos2d;

GreenEnemyTankBuilder::GreenEnemyTankBuilder() {
}

GreenEnemyTankBuilder::~GreenEnemyTankBuilder() {
}

void GreenEnemyTankBuilder::createNewTankModel() {
	TankBuilder::createNewTankModel();
	mTankModel->setCurrArmor(4);
	mTankModel->setMaxArmor(4);
	mTankModel->setBounty(5);
	mTankModel->setTeamIndex(2);
}

void GreenEnemyTankBuilder::addPlatform() {
	PlatformModelRef platformModel = std::make_shared<PlatformModel>();
	platformModel->setType(PlatformModel::Type::GREEN);
	platformModel->setMoveSpeed(10.0f);
	platformModel->setRotationSpeed(200.0f);
	platformModel->setOwner(mTankModel);
	platformModel->setTeamIndex(2);
	mTankModel->setPlatform(platformModel);
}

void GreenEnemyTankBuilder::addCannon() {
	SingleCannonModelRef cannonModel = std::make_shared<SingleCannonModel>();
	cannonModel->setStartBulletSpeed(PLAYER_BULLET_SPEED / 1.5f);
	cannonModel->setReloadTime(1.0f);
	cannonModel->setOwner(mTankModel);
	cannonModel->setTeamIndex(2);
	mTankModel->setCannon(cannonModel);
}
#include "PlayerTankBuilder.h"
#include "Models/Cannons/SingleCannonModel.h"
#include "Models/Cannons/DualCannonModel.h"
#include "Models/Cannons/CrossCannonModel.h"
#include "common.h"

using namespace cocos2d;

PlayerTankBuilder::PlayerTankBuilder() {
}

PlayerTankBuilder::~PlayerTankBuilder() {
}

void PlayerTankBuilder::createNewTankModel() {
	TankBuilder::createNewTankModel();
	mTankModel->setIsPlayerTank(true);
	mTankModel->setCurrArmor(PLAYER_CURR_ARMOR);
	mTankModel->setMaxArmor(PLAYER_MAX_ARMOR);
	mTankModel->setTeamIndex(1);
}

void PlayerTankBuilder::addPlatform() {
	PlatformModelRef platformModel = std::make_shared<PlatformModel>();
	platformModel->setType(PlatformModel::Type::OLIVE);
	platformModel->setMoveSpeed(PLAYER_MOVE_SPEED);
	platformModel->setRotationSpeed(PLAYER_PLATFORM_ROTATION_SPEED);
	platformModel->setOwner(mTankModel);
	platformModel->setTeamIndex(1);
	mTankModel->setPlatform(platformModel);
}

void PlayerTankBuilder::addCannon() {
	/*
	CrossCannonModelRef cannonModel = std::make_shared<CrossCannonModel>();
	cannonModel->setStartBulletSpeed(500.0f);
	cannonModel->setReloadTime(0.2f);
	cannonModel->setOwner(mTankModel);
	
	DualCannonModelRef cannonModel = std::make_shared<DualCannonModel>();
	cannonModel->setStartBulletSpeed(500.0f);
	cannonModel->setReloadTime(0.15f);
	cannonModel->setOwner(mTankModel);
	*/

	SingleCannonModelRef cannonModel = std::make_shared<SingleCannonModel>();
	cannonModel->setStartBulletSpeed(PLAYER_BULLET_SPEED);
	cannonModel->setReloadTime(1.0f);
	cannonModel->setOwner(mTankModel);
	cannonModel->setTeamIndex(1);
	mTankModel->setCannon(cannonModel);
}
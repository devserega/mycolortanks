#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _RED_ENEMY_TANK_BUILDER_H_
#define _RED_ENEMY_TANK_BUILDER_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "Builders/TankBuilder.h"
//----------------------------------------------------------------------------------------------------
class RedEnemyTankBuilder
	: public TankBuilder
{
public:
	RedEnemyTankBuilder();
	virtual ~RedEnemyTankBuilder();

	virtual void createNewTankModel() override;
	virtual void addPlatform();
	virtual void addCannon();
};
//----------------------------------------------------------------------------------------------------
#endif // _RED_ENEMY_TANK_BUILDER_H_
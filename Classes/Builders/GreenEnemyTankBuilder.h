#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _GREEN_ENEMY_TANK_BUILDER_H_
#define _GREEN_ENEMY_TANK_BUILDER_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "TankBuilder.h"
//----------------------------------------------------------------------------------------------------
class GreenEnemyTankBuilder
	: public TankBuilder
{
	public:
		GreenEnemyTankBuilder();
		virtual ~GreenEnemyTankBuilder();

		virtual void createNewTankModel() override;
		virtual void addPlatform();
		virtual void addCannon();
};
//----------------------------------------------------------------------------------------------------
#endif // _GREEN_ENEMY_TANK_BUILDER_H_
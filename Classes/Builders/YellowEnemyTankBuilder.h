#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _YELLOW_ENEMY_TANK_BUILDER_H_
#define _YELLOW_ENEMY_TANK_BUILDER_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "Builders/TankBuilder.h"
//----------------------------------------------------------------------------------------------------
class YellowEnemyTankBuilder
	: public TankBuilder
{
	public:
		YellowEnemyTankBuilder();
		virtual ~YellowEnemyTankBuilder();

		virtual void createNewTankModel() override;
		virtual void addPlatform();
		virtual void addCannon();
};
//----------------------------------------------------------------------------------------------------
#endif // _YELLOW_ENEMY_TANK_BUILDER_H_
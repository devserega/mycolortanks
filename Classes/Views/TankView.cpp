#include "TankView.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "utilities.h"
#include "BulletView.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
using namespace ui;
//----------------------------------------------------------------------------------------------------
TankView* TankView::create() {
	TankView* pRet = new (std::nothrow) TankView();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		return nullptr;
	}
}

TankView* TankView::createDefault(TankModelRef tankModel) {
	static cocos2d::Data data;
	if (0 == data.getSize()) {
		CSLoader::getInstance()->registReaderObject("TankViewReader", (ObjectFactory::Instance) TankViewReader::getInstance);
		data = FileUtils::getInstance()->getDataFromFile("Prefabs/Tank.csb");
	}

	auto rootNode = static_cast<TankView*>(CSLoader::createNode(data, TankViewReader::sm_nodeLoadCallback));
	rootNode->mRootNode = rootNode;
	rootNode->setContentSize(Director::getInstance()->getWinSize());
	ui::Helper::doLayout(rootNode);

	rootNode->mTankModel = tankModel;

	if (tankModel) {
		auto platformModel = tankModel->getPlatformModel();
		if (platformModel) {
			platformModel->addPlatformModelListener(rootNode);
			rootNode->setPosition(platformModel->getPosition());

			auto platform_slot = nmUtils::findChildNode<Node>(rootNode, "platform_slot");
			if (platform_slot) {
				float platformScale = platform_slot->getScale();
				PlatformModel::Type tankPlatformType = platformModel->getType();

				for (size_t i = 0; i < static_cast<size_t>(PlatformModel::Type::COUNT); ++i) {
					PlatformModel::Type platformType = static_cast<PlatformModel::Type>(i);
					char platformName[255];
					sprintf(platformName, "platform%d", (i + 1));

					rootNode->mPlatformViews[i] = nmUtils::findChildNode<ImageView>(platform_slot, platformName);						
					if (rootNode->mPlatformViews[i] && platformType == tankPlatformType){
						rootNode->mPlatformViews[i]->setVisible(true);
						auto cz = rootNode->mPlatformViews[i]->getContentSize();
						platformModel->setBoundingBox(cz*platformScale);
						rootNode->mPlatformViews[i]->setRotation(-platformModel->getRotation());
					}
					else if(rootNode->mPlatformViews[i]){
						rootNode->mPlatformViews[i]->setVisible(false);
					}
				}	
			}
		}

		auto cannon_slot = nmUtils::findChildNode<Node>(rootNode, "tower_slot");
		if (cannon_slot) {
			for (size_t i = 0; i < static_cast<size_t>(CannonModel::Type::COUNT); ++i) {
				CannonModel::Type cannonType = static_cast<CannonModel::Type>(i);
				char cannonName[255];
				sprintf(cannonName, "cannon%d", (i + 1));

				rootNode->mTowerViews[i] = nmUtils::findChildNode<ImageView>(cannon_slot, cannonName);
				if (rootNode->mTowerViews[i]) {
					std::vector<Vec2> bulletSpawnPoints;

					for (size_t j = 1; j <= 4; ++j) {
						char spawnPointName[255];
						sprintf(spawnPointName, "bullet_spawn%d", j);

						auto spawnPoint = nmUtils::findChildNode<Node>(rootNode->mTowerViews[i], spawnPointName);
						if (spawnPoint) {
							Vec2 pos = spawnPoint->getPosition();
							Vec2 worldPos = rootNode->mTowerViews[i]->convertToWorldSpace(pos);
							Vec2 localPos = cannon_slot->convertToNodeSpace(worldPos);
							bulletSpawnPoints.push_back(localPos);
						}
					}

					if (bulletSpawnPoints.size() > 0) {
						rootNode->mBulletSpawnPoints[i] = bulletSpawnPoints;
					}
				}
			}
			rootNode->setCannonModelView(*tankModel.get());
		}

		tankModel->addBaseModelListener(rootNode);
		tankModel->addTankModelListener(rootNode);
	}

	rootNode->mArmorBar = nmUtils::findChildNode<LoadingBar>(rootNode, "armor_bar");

	return (TankView*) rootNode;
}

TankView::TankView()
	: mRootNode(nullptr)
	, mTankModel(nullptr)
	, mArmorBar(nullptr)
{
}

TankView::~TankView() {
	if (mTankModel) {
		mTankModel->removeBaseModelListener(this);
		mTankModel->removeTankModelListener(this);
	}
}

bool TankView::init() {
	if (!Parent::init())
		return false;

	return true;
}

bool TankView::init(Node* rootNode) {
	if (!rootNode)
		return false;

	return true;
}

void TankView::setColor(const Color3B& color) {
	auto towerView = getTowerView();
	if (towerView) {
		towerView->setColor(color);
	}

	auto platformView = getPlatformView();
	if (platformView) {
		platformView->setColor(color);
	}
}

void TankView::onSetPlatform(TankModel& tankModel) {

}

void TankView::onSetCannon(TankModel& tankModel) {
	setCannonModelView(tankModel);
}

void TankView::onChangedArmor(TankModel& tankModel) {
	float percent = (float) tankModel.getCurrArmor() / (float) tankModel.getMaxArmor() * 100;
	mArmorBar->setPercent(percent);
}

void TankView::onPlatformRotated(float newAngleDegree) {
	auto platformView = getPlatformView();
	if (platformView) {
		platformView->setRotation(-newAngleDegree);
	}
}

void TankView::onPlatformMoved(const Vec2& deltaMove) {
	if (!mTankModel)
		return;

	auto platformModel = mTankModel->getPlatformModel();
	if (platformModel) {
		auto pos = platformModel->getPosition();
		setPosition(pos);
	}
}

void TankView::onCannonRotated(float newAngleDegree) {
	auto towerView = getTowerView();
	if (towerView) {
		towerView->setRotation(-newAngleDegree);
	}
}

void TankView::onCannonFired(std::vector<BulletModelRef> bullets) {
	auto map = getParent();
	if (map) {
		for (size_t i = 0; i < bullets.size(); ++i) {
			auto bulletView = BulletView::createDefault(bullets[i]);
			map->addChild(bulletView);
		}
	}
}

void TankView::onDestroyed(BaseModel* mode) {
	if (mTankModel) {
		mTankModel->removeBaseModelListener(this);
	}
	this->removeFromParentAndCleanup(true);
}

ImageView* TankView::getPlatformView() {
	if (mTankModel) {
		auto platformModel = mTankModel->getPlatformModel();
		if (platformModel) {
			size_t platformTypeCode = static_cast<size_t>(platformModel->getType());
			return mPlatformViews[platformTypeCode];
		}
	}

	return nullptr;
}

ImageView* TankView::getTowerView() {
	if (mTankModel) {
		auto cannonModel = mTankModel->getCannonModel();
		if (cannonModel) {
			size_t towerTypeCode = static_cast<size_t>(cannonModel->getType());
			return mTowerViews[towerTypeCode];
		}
	}

	return nullptr;
}

void TankView::setCannonModelView(TankModel& tankModel) {
	auto cannonModel = tankModel.getCannonModel();
	if (cannonModel) {
		cannonModel->addCannonModelListener(this);

		CannonModel::Type tankCannonType = cannonModel->getType();
		for (size_t i = 0; i < static_cast<size_t>(CannonModel::Type::COUNT); ++i) {			
			CannonModel::Type cannonType = static_cast<CannonModel::Type>(i);
			if (tankCannonType == cannonType) {
				mTowerViews[i]->setVisible(true);
				mTowerViews[i]->setRotation(cannonModel->getRotation());
				cannonModel->setBulletsSpawnPoints(mBulletSpawnPoints[i]);
			}
			else {
				mTowerViews[i]->setVisible(false);
			}
		}
	}
}

//----------------------------------------------------------------------------------------------------
TankView* TankViewReader::sm_pTmpNode = nullptr;
void TankViewReader::sm_nodeLoadCallback(cocos2d::Ref* pRef) {
	if (TankViewReader::sm_pTmpNode) {
		cocos2d::Node * pNode = dynamic_cast<cocos2d::Node*>(pRef);
		if (!pNode) {
			return;
		}

		// ...
	}
}

static TankViewReader* _instanceTankViewReader = nullptr;
TankViewReader* TankViewReader::getInstance() {
	if (!_instanceTankViewReader) {
		_instanceTankViewReader = new TankViewReader();
	}
	return _instanceTankViewReader;
}

void TankViewReader::purge() {
	CC_SAFE_DELETE(_instanceTankViewReader);
}

Node* TankViewReader::createNodeWithFlatBuffers(const flatbuffers::Table* nodeTankView) {
	TankView* node = TankView::create();
	sm_pTmpNode = node;
	setPropsWithFlatBuffers(node, nodeTankView);
	return node;
}
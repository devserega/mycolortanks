#include "BulletView.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "utilities.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
using namespace ui;
//----------------------------------------------------------------------------------------------------
BulletView* BulletView::create() {
	BulletView* pRet = new (std::nothrow) BulletView();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		return nullptr;
	}
}

BulletView* BulletView::createDefault(BulletModelRef bulletModel) {
	static cocos2d::Data data;
	if (0 == data.getSize()) {
		CSLoader::getInstance()->registReaderObject("BulletViewReader", (ObjectFactory::Instance) BulletViewReader::getInstance);
		data = FileUtils::getInstance()->getDataFromFile("Prefabs/Bullet.csb");
	}

	auto rootNode = static_cast<BulletView*>(CSLoader::createNode(data, BulletViewReader::sm_nodeLoadCallback));
	rootNode->mRootNode = rootNode;
	rootNode->setContentSize(Director::getInstance()->getWinSize());
	ui::Helper::doLayout(rootNode);

	if (bulletModel) {
		bulletModel->addBaseModelListener(rootNode);
		rootNode->mBulletModel = bulletModel;

		auto cannonball = nmUtils::findChildNode<ImageView>(rootNode, "cannonball");
		if (cannonball) {
			float scale = cannonball->getScale();
			auto cz = cannonball->getContentSize();
			bulletModel->setBoundingBox(cz*scale);
			cannonball->setColor(bulletModel->getColor());
		}

		rootNode->mRootNode->setPosition(bulletModel->getPosition());
		rootNode->mRootNode->setRotation(bulletModel->getRotation());
		rootNode->setScale(bulletModel->getScale());
	}

	return (BulletView*) rootNode;
}

BulletView::BulletView()
	: mRootNode(nullptr)
	, mBulletModel(nullptr)
{
}

BulletView::~BulletView() {
	if (mBulletModel) {
		mBulletModel->removeBaseModelListener(this);
	}
}

bool BulletView::init() {
	if (!Parent::init())
		return false;

	return true;
}

bool BulletView::init(Node* rootNode) {
	if (!rootNode)
		return false;

	return true;
}

void BulletView::onMoved(BaseModel* model, const Vec2& deltaMove) {
	if (!mBulletModel)
		return;

	auto pos = mBulletModel->getPosition();
	setPosition(pos);
}

void BulletView::onDestroyed(BaseModel* mode) {
	if (mBulletModel) {
		mBulletModel->removeBaseModelListener(this);
	}
	this->removeFromParentAndCleanup(true);
}

//----------------------------------------------------------------------------------------------------
BulletView* BulletViewReader::sm_pTmpNode = nullptr;
void BulletViewReader::sm_nodeLoadCallback(cocos2d::Ref* pRef) {
	if (BulletViewReader::sm_pTmpNode) {
		cocos2d::Node * pNode = dynamic_cast<cocos2d::Node*>(pRef);
		if (!pNode) {
			return;
		}

		// ...
	}
}

static BulletViewReader* _instanceBulletViewReader = nullptr;
BulletViewReader* BulletViewReader::getInstance() {
	if (!_instanceBulletViewReader) {
		_instanceBulletViewReader = new BulletViewReader();
	}
	return _instanceBulletViewReader;
}

void BulletViewReader::purge() {
	CC_SAFE_DELETE(_instanceBulletViewReader);
}

Node* BulletViewReader::createNodeWithFlatBuffers(const flatbuffers::Table* nodeBulletView) {
	BulletView* node = BulletView::create();
	sm_pTmpNode = node;
	setPropsWithFlatBuffers(node, nodeBulletView);
	return node;
}
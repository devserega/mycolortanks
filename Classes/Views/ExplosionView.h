#ifndef _EXPLOSION_VIEW_H_
#define _EXPLOSION_VIEW_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "ui/CocosGUI.h"
//----------------------------------------------------------------------------------------------------
class ExplosionView
	: public cocos2d::Node
{
	public:
		static ExplosionView* create();
		virtual void onEnter() override;
		virtual void onExit() override;

		virtual void setColor(const cocos2d::Color3B& color) override;

	protected:
		ExplosionView();
		virtual ~ExplosionView();
		virtual bool init() override;

		Node* mExplosionNode;
		cocos2d::ui::ImageView* mExplosionImage;
};
//----------------------------------------------------------------------------------------------------
#endif // _EXPLOSION_VIEW_H_ 

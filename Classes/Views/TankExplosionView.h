#ifndef _TANK_EXPLOSION_VIEW_H_
#define _TANK_EXPLOSION_VIEW_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "ui/CocosGUI.h"
//----------------------------------------------------------------------------------------------------
class TankExplosionView
	: public cocos2d::Node
{
	public:
		static TankExplosionView* create();
		virtual void onEnter() override;
		virtual void onExit() override;

	protected:
		TankExplosionView();
		virtual ~TankExplosionView();
		virtual bool init() override;

		Node* mExplosionNode;
		cocos2d::ui::ImageView* mExplosionImage;
};
//----------------------------------------------------------------------------------------------------
#endif // _TANK_EXPLOSION_VIEW_H_ 

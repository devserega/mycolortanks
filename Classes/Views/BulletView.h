#pragma once
#ifndef _BULLET_VIEW_H_
#define _BULLET_VIEW_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "editor-support/cocostudio/WidgetCallBackHandlerProtocol.h"
#include "editor-support/cocostudio/WidgetReader/NodeReader/NodeReader.h"

#include "Models/WorldModel.h"
//----------------------------------------------------------------------------------------------------
class BulletView
	: public cocos2d::ui::Layout
	, public BaseModelListener
{
		friend class BulletViewReader;
		typedef cocos2d::ui::Layout Parent;

	public:
		BulletView();
		virtual ~BulletView();
		static BulletView* createDefault(BulletModelRef bulletModel);
		static BulletView* create();
		bool init();
		bool init(cocos2d::Node* rootNode);

		/* BaseModelListener */
		void onMoved(BaseModel* model, const cocos2d::Vec2& deltaMove) override;
		void onDestroyed(BaseModel* mode) override;

	protected:
		BulletView* mRootNode;
		BulletModelRef mBulletModel;
};
//----------------------------------------------------------------------------------------------------
class BulletViewReader
	: public cocostudio::NodeReader
{
	public:
		static BulletView* sm_pTmpNode;
		static void sm_nodeLoadCallback(cocos2d::Ref* pRef);

		BulletViewReader() {};
		~BulletViewReader() {};
		static BulletViewReader* getInstance();
		static void purge();
		cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeTankView);
};
//----------------------------------------------------------------------------------------------------
#endif // _BULLET_VIEW_H_
#include "TankExplosionView.h"
#include "cocostudio/CocoStudio.h"
#include "utilities.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
using namespace ui;
//----------------------------------------------------------------------------------------------------
TankExplosionView::TankExplosionView()
	: mExplosionNode(nullptr)
	, mExplosionImage(nullptr)
{
}

TankExplosionView::~TankExplosionView() {
	if (mExplosionNode) {
		mExplosionNode->removeFromParent();
		mExplosionNode->release();
		mExplosionNode = nullptr;
	}
}

TankExplosionView* TankExplosionView::create()
{
	TankExplosionView* explosionView = new TankExplosionView();
	if (explosionView->init()) {
		explosionView->autorelease();
		return explosionView;
	}
	CC_SAFE_DELETE(explosionView);
	return nullptr;
}

bool TankExplosionView::init()
{
	if (!Node::init())
		return false;

	mExplosionNode = CSLoader::createNode("Prefabs/TankExplosion.csb");
	if (!mExplosionNode) return true;
	mExplosionNode->retain();

	mExplosionImage = nmUtils::findChildNode<ImageView>(mExplosionNode, "explosion");

	auto tl = CSLoader::createTimeline("Prefabs/TankExplosion.csb");
	if (tl)
	{
		auto endCallFunc = [this]() {
			removeFromParentAndCleanup(true);
		};

		mExplosionNode->runAction(tl);
		tl->setAnimationEndCallFunc("default", endCallFunc);
		tl->play("default", false);
	}

	mExplosionNode->setPosition(Vec2::ZERO);
	addChild(mExplosionNode);

	setCascadeOpacityEnabled(false);
	setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	return true;
}


void TankExplosionView::onEnter()
{
	Node::onEnter();
	if (mExplosionNode)
	{
		mExplosionNode->setVisible(true);

		if (!mExplosionNode->getParent())
		{
			mExplosionNode->setPosition(Vec2::ZERO);
			addChild(mExplosionNode);
		}
	}
}

void TankExplosionView::onExit()
{
	if (mExplosionNode)
	{
		mExplosionNode->setVisible(false);
	}
	Node::onExit();
}
#pragma once
#ifndef _TANK_VIEW_H_
#define _TANK_VIEW_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "editor-support/cocostudio/WidgetCallBackHandlerProtocol.h"
#include "editor-support/cocostudio/WidgetReader/NodeReader/NodeReader.h"

#include "Models/WorldModel.h"
#include <array>
//----------------------------------------------------------------------------------------------------
class TankView
	: public cocos2d::ui::Layout
	, public ITankModelListener
	, public IPlatformModelListener
	, public ICannonModelListener
	, public BaseModelListener
{
		friend class TankViewReader;
		typedef cocos2d::ui::Layout Parent;

	public:
		TankView();
		virtual ~TankView();
		static TankView* createDefault(TankModelRef tankModel);
		static TankView* create();
		bool init();
		bool init(cocos2d::Node* rootNode);
		void setColor(const cocos2d::Color3B& color) override;
		cocos2d::ui::ImageView* getPlatformView();
		cocos2d::ui::ImageView* getTowerView();

		/* ITankModelListener */
		void onSetPlatform(TankModel& tankModel) override;
		void onSetCannon(TankModel& tankModel) override;
		void onChangedArmor(TankModel& tankModel) override;
		/* IPlatformrModelListener */
		void onPlatformRotated(float angle);
		void onPlatformMoved(const cocos2d::Vec2& deltaMove);
		/* ICannonModelListener */
		void onCannonRotated(float angle) override;
		void onCannonFired(std::vector<BulletModelRef> bullets) override;
		/* BaseModelListener */
		void onDestroyed(BaseModel* mode) override;

	protected:
		TankView* mRootNode;
		TankModelRef mTankModel;
		cocos2d::ui::LoadingBar* mArmorBar;
		std::array<cocos2d::ui::ImageView*, static_cast<size_t>(PlatformModel::Type::COUNT)> mPlatformViews;
		std::array<cocos2d::ui::ImageView*, static_cast<size_t>(CannonModel::Type::COUNT)> mTowerViews;
		std::array<std::vector<cocos2d::Vec2>, static_cast<size_t>(CannonModel::Type::COUNT)> mBulletSpawnPoints;

		void setCannonModelView(TankModel& tankModel);
};
//--------------------------------------------------------------------------------------------------------------------------
class TankViewReader
	: public cocostudio::NodeReader
{
public:
	static TankView* sm_pTmpNode;
	static void sm_nodeLoadCallback(cocos2d::Ref* pRef);

	TankViewReader() {};
	~TankViewReader() {};
	static TankViewReader* getInstance();
	static void purge();
	cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeTankView);
};
//----------------------------------------------------------------------------------------------------
#endif // _TANK_VIEW_H_
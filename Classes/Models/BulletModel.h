#pragma once
#ifndef _BULLET_MODEL_H_
#define _BULLET_MODEL_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "BaseModel.h"
#include "utilities.h"
//----------------------------------------------------------------------------------------------------
class BulletModel;
typedef std::shared_ptr<BulletModel> BulletModelRef;
//----------------------------------------------------------------------------------------------------
class BulletModel 
	: public BaseModel
{
	public:
		BulletModel();
		virtual ~BulletModel();
		virtual void update(float dt) override;
		virtual void setMoveSpeed(float speed) { mMoveSpeed = speed; }
		virtual std::vector<cocos2d::Point> getBoundingBox(const cocos2d::Point& pos, float angleDegree); 
		void setPosition(const cocos2d::Vec2& newPosition) override;
		virtual void setLifetime(float lifetime) { mLifetime = lifetime; }
		virtual void setDamage(float damage) { mDamage = damage; }
		virtual float getDamage() { return mDamage; }
		virtual void setScale(float scale) { mScale = scale; }
		virtual float getScale() { return mScale; }

	protected:
		float mMoveSpeed;	
		float mLifetime;
		float mDamage;
		float mScale;
};
//----------------------------------------------------------------------------------------------------
#endif // _BULLET_MODEL_H_
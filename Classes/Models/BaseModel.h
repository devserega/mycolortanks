#pragma once
#ifndef __BASE_MODEL_H__
#define __BASE_MODEL_H__
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
//----------------------------------------------------------------------------------------------------
class WorldModel;
typedef std::shared_ptr<WorldModel> WorldModelRef;
class BulletModel;
typedef std::shared_ptr<BulletModel> BulletModelRef;
class BaseModel;
typedef std::shared_ptr<BaseModel> BaseModelRef;
//----------------------------------------------------------------------------------------------------
class BaseModelListener {
	public:
		virtual void onDestroyed(BaseModel* model) {}
		virtual void onMoved(BaseModel* model, const cocos2d::Vec2& deltaMove) {}
		virtual void onCollisionDetected(BaseModel* model1, BaseModel* model2) {}
};
//----------------------------------------------------------------------------------------------------
class BaseModel
{
	public:
		enum class Type : size_t{
			PLATFORM = 0,
			CANNON = 1,
			BULLET = 2,
			TANK = 3,
			COUNT = 4,
			NONE
		};

		typedef std::vector<cocos2d::Vec2> TVertexes;
		typedef std::unordered_map<unsigned long long, std::tuple<BaseModel*, BaseModel*>> TCollisions;

		BaseModel(Type type);
		virtual ~BaseModel();
		size_t getID() { return mID; }
		virtual void update(float dt) {}
		virtual WorldModelRef getWorldModel() { return mWorldModel; }
		virtual void setWorldModel(WorldModelRef worldModel) { mWorldModel = worldModel; }
		void setRotation(float angle) { mRotationAngleDegree = angle; }
		virtual float getRotation() { return mRotationAngleDegree; }
		virtual void setPosition(const cocos2d::Vec2& pos) { mPosition = pos; }
		virtual const cocos2d::Vec2& getPosition() { return mPosition; }
		virtual void setBoundingBox(const cocos2d::Size& boundingBox) { mBoundingBoxSize = boundingBox; }
		virtual TVertexes getBoundingBox(const cocos2d::Point& pos, float angleDegree);
		virtual void addBaseModelListener(BaseModelListener* listener);
		virtual void removeBaseModelListener(BaseModelListener* listener);
		bool isDestroyed() { return mDestroyed; }
		Type getType() { return mType; }
		virtual void destroy();
		virtual bool checkCollisionWithWorldBorders(const cocos2d::Point& pos);
		virtual bool checkCollisionWithOtherModel(const cocos2d::Point& pos, float angleDegree, BaseModelRef model);
		virtual void setOwner(BaseModelRef owner);
		virtual BaseModelRef getOwner();
		virtual void setColor(cocos2d::Color3B& color) { mColor = color; }
		virtual const cocos2d::Color3B& getColor() {return mColor;}
		virtual void setTeamIndex(size_t teamIndex) { mTeamIndex = teamIndex; }
		virtual size_t getTeamIndex() { return mTeamIndex; }

	protected:
		size_t							mID;
		WorldModelRef					mWorldModel;
		float							mRotationAngleDegree;
		cocos2d::Vec2					mPosition;
		cocos2d::Size					mBoundingBoxSize;
		std::vector<BaseModelListener*> mBaseModelListeners;
		bool							mDestroyed;
		Type							mType;
		BaseModelRef					mOwner;
		cocos2d::Color3B				mColor;
		size_t							mTeamIndex;

		float calculateAngles(float &startAngle, float dstAngle);
		bool linesIntersection(const cocos2d::Vec2& p10, const cocos2d::Vec2& p11, const cocos2d::Vec2& p20, const cocos2d::Vec2& p21);
		// function to find if given point lies inside a given rectangle or not. 
		bool pointInsideRectangle(const cocos2d::Vec2& xy1, const cocos2d::Vec2& xy2, const cocos2d::Vec2& point);
};
//----------------------------------------------------------------------------------------------------
#endif // __BASE_MODEL_H__
#include "TankModel.h"
#include "WorldModel.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
using namespace std;
//----------------------------------------------------------------------------------------------------
TankModel::TankModel()
	: BaseModel(BaseModel::Type::TANK)
	, mPlatformModel(nullptr)
	, mCannonModel(nullptr)
	, mModelUpdated(false)
	, mIsPlayerTank(false)
	, mTankColor(255,255,255)
	, mBounty(0)
{
}

TankModel::~TankModel() {
	mTankModelListeners.clear();
}

void TankModel::update(float dt) {
	if (mPlatformModel && mWorldModel) {
		size_t collisionCounter = 0;

		if (mPlatformModel->getMoveDirection() != PlatformModel::MoveDirection::NONE ||
			mPlatformModel->getRotateDirection() != PlatformModel::RotateDirection::NONE) 
		{
			// get next movement attributes
			std::tuple<float, Vec2> attr = mPlatformModel->getNextMovementAttributes(dt);
			float newTankAngle = std::get<0>(attr);
			Vec2 newTankPos = std::get<1>(attr);
			Vec2 currTankPos = this->getPosition();

			// check collsions with world borders
			bool forcePlatformUpdate = false;
			if (!mPlatformModel->checkCollisionWithWorldBorders(newTankPos)) {
				// check collisons with other models
				auto modelList = mWorldModel->getModelsList();
				for (size_t j = 0; j < modelList.size(); ++j) {
					BaseModelRef model = modelList[j];

					if (!model)
						continue;

					if (model.get() != this && this != model->getOwner().get()) {
						bool isCollision = checkCollisionWithOtherModel(newTankPos, newTankAngle, model);
						if (isCollision) {
							if (model->getType() == BaseModel::Type::TANK) {
								auto modelPos = model->getPosition();
								float currDistance = sqrt(pow(modelPos.x - currTankPos.x, 2) + pow(modelPos.y - currTankPos.y, 2));
								float newDistance = sqrt(pow(modelPos.x - newTankPos.x, 2) + pow(modelPos.y - newTankPos.y, 2));
								if (newDistance < currDistance)
									collisionCounter++;
							}

							mWorldModel->onCollisionDetected(this, model.get());
						}
					}	
				}

				if (collisionCounter == 0) {
					mPlatformModel->update(dt);
				}
			}
		}
	}

	if (mCannonModel && mPlatformModel) {
		auto pos = mPlatformModel->getPosition();
		mCannonModel->setPosition(pos);
		mCannonModel->update(dt);
	}
}

void TankModel::setPlatform(PlatformModelRef platformModel) {
	mPlatformModel = platformModel;

	std::vector<ITankModelListener*>::iterator it = mTankModelListeners.begin();
	while (it != mTankModelListeners.end()){
		ITankModelListener* listener = *it;
		if (listener){
			listener->onSetPlatform(*this);
			it++;
		}
		else it = mTankModelListeners.erase(it);
	}
}

void TankModel::setCannon(CannonModelRef cannonModel) {
	mCannonModel = cannonModel;

	std::vector<ITankModelListener*>::iterator it = mTankModelListeners.begin();
	while (it != mTankModelListeners.end()){
		ITankModelListener* listener = *it;
		if (listener){
			listener->onSetCannon(*this);
			it++;
		}
		else it = mTankModelListeners.erase(it);
	}
}

void TankModel::addTankModelListener(ITankModelListener* listener) {
	if (!listener) return;

	std::vector<ITankModelListener*>::iterator it;
	it = std::find(mTankModelListeners.begin(), mTankModelListeners.end(), listener);
	if (it == mTankModelListeners.end())
		mTankModelListeners.push_back(listener);
}

void TankModel::removeTankModelListener(ITankModelListener* listener) {
	if (!listener) return;

	std::vector<ITankModelListener*>::iterator it;
	it = std::find(mTankModelListeners.begin(), mTankModelListeners.end(), listener);
	if (it != mTankModelListeners.end())
		*it = nullptr;
}

void TankModel::setWorldModel(WorldModelRef worldModel) {
	BaseModel::setWorldModel(worldModel);

	if (mCannonModel) {
		mCannonModel->setWorldModel(worldModel);
	}

	if (mPlatformModel) {
		mPlatformModel->setWorldModel(worldModel);
	}
}

vector<Point> TankModel::getBoundingBox(const Point& pos, float angleDegree) {
	return mPlatformModel->getBoundingBox(mPlatformModel->getPosition(), mPlatformModel->getRotation());
}

const Vec2& TankModel::getPosition() {
	if (mPlatformModel) {
		return mPlatformModel->getPosition();
	}
	else {
		return BaseModel::getPosition();
	}
}

float TankModel::getRotation() {
	if (mPlatformModel) {
		return mPlatformModel->getRotation();
	}
	else {
		return BaseModel::getRotation();
	}
}

void TankModel::damage(int damage) {
	bool tankDestroyed = false;

	if ((mCurrArmor - damage) > FLT_MIN) {
		mCurrArmor -= damage;
	}
	else {
		mCurrArmor = 0;
		tankDestroyed = true;
	}

	// refresh armor bar on tank view
	std::vector<ITankModelListener*>::iterator it = mTankModelListeners.begin();
	while (it != mTankModelListeners.end()){
		ITankModelListener* listener = *it;
		if (listener){
			listener->onChangedArmor(*this);
			it++;
		}
		else it = mTankModelListeners.erase(it);
	}

	if (tankDestroyed) {
		destroy();
	}
}

void TankModel::setPosition(const Vec2& pos) {
	BaseModel::setPosition(pos);

	if (mPlatformModel) {
		mPlatformModel->setPosition(pos);
	}
}
#include "PlatformModel.h"
#include "WorldModel.h"

using namespace cocos2d;

PlatformModel::PlatformModel()
	: BaseModel(BaseModel::Type::PLATFORM)
	, mMoveSpeed(0.f)
	, mRotationSpeed(0.f)
	, mMoveDirection(MoveDirection::NONE)
	, mRotateDirection(RotateDirection::NONE)
{
}

PlatformModel::~PlatformModel() {
	mPlatformModelListeners.clear();
}

std::tuple<float, Vec2> PlatformModel::processMoveAction(float dt, float moveSpeed) {
	Vec2 newPosition = mPosition + Vec2(moveSpeed * dt, 0);
	nmUtils::rotate(newPosition, mPosition, mRotationAngleDegree);
	return std::make_tuple(mRotationAngleDegree, newPosition);
}

std::tuple<float, Vec2> PlatformModel::processRotateAction(float dt, float rotationSpeed) {
	float angleDegree = mRotationAngleDegree + rotationSpeed * dt;
	return std::make_tuple(angleDegree, mPosition);
}

std::tuple<float, Vec2> PlatformModel::processAction(float dt, float targetAngleDegree, float moveSpeed) {
	std::tuple<float, Vec2> attr;

	float diffAngle = calculateAngles(mRotationAngleDegree, targetAngleDegree);
	if (floor(fabs(diffAngle)) < FLT_EPSILON) {
		attr = processMoveAction(dt, mMoveSpeed);
	}
	else if (diffAngle > 0.0f) {
		attr = processRotateAction(dt, mRotationSpeed);
	}
	else if (diffAngle < 0.0f) {
		attr = processRotateAction(dt, -mRotationSpeed);
	}

	return attr;
}

std::tuple<float, Vec2> PlatformModel::getNextMovementAttributes(float dt) {
	std::tuple<float, Vec2> attr = std::make_tuple(mRotationAngleDegree,mPosition);

	if (mMoveDirection == MoveDirection::FORWARD) {
		attr = processAction(dt, mTargetAngleDegree, mMoveSpeed);
	}

	return attr;
}

void PlatformModel::update(float dt) {
	std::tuple<float, Vec2> attr = getNextMovementAttributes(dt);
	float newAngle = std::get<0>(attr);
	Vec2 newPos = std::get<1>(attr);

	if (newAngle != mRotationAngleDegree) {
		setRotation(newAngle);
	}

	if (newPos != mPosition) {
		setPosition(newPos);
	}
}

void PlatformModel::addPlatformModelListener(IPlatformModelListener* listener) {
	if (!listener) return;

	std::vector<IPlatformModelListener*>::iterator it;
	it = std::find(mPlatformModelListeners.begin(), mPlatformModelListeners.end(), listener);
	if (it == mPlatformModelListeners.end())
		mPlatformModelListeners.push_back(listener);
}

void PlatformModel::removePlatformModelListener(IPlatformModelListener* listener) {
	if (!listener) return;

	std::vector<IPlatformModelListener*>::iterator it;
	it = std::find(mPlatformModelListeners.begin(), mPlatformModelListeners.end(), listener);
	if (it != mPlatformModelListeners.end())
		*it = nullptr;
}

void PlatformModel::setRotation(float rotationAngleDegree) {
	rotationAngleDegree = floor(rotationAngleDegree);

	BaseModel::setRotation(rotationAngleDegree);

	std::vector<IPlatformModelListener*>::iterator it = mPlatformModelListeners.begin();
	while (it != mPlatformModelListeners.end())
	{
		IPlatformModelListener* listener = *it;
		if (listener)
		{
			listener->onPlatformRotated(rotationAngleDegree);
			it++;
		}
		else it = mPlatformModelListeners.erase(it);
	}
}

void PlatformModel::setPosition(const Vec2& newPosition) {
	Vec2 deltaMove = newPosition - mPosition;

	BaseModel::setPosition(newPosition);

	std::vector<IPlatformModelListener*>::iterator it = mPlatformModelListeners.begin();
	while (it != mPlatformModelListeners.end())
	{
		IPlatformModelListener* listener = *it;
		if (listener)
		{
			listener->onPlatformMoved(deltaMove);
			it++;
		}
		else it = mPlatformModelListeners.erase(it);
	}
}

void PlatformModel::move(MoveDirection dir) {
	mMoveDirection = dir;
}

void PlatformModel::rotate(float targetAngleDegree) {
	mTargetAngleDegree = targetAngleDegree;
}

std::vector<Point> PlatformModel::getBoundingBox(const Point& pos, float angleDegree) {
	float halfWidth = mBoundingBoxSize.width / 2;
	float halfHeight = mBoundingBoxSize.height / 2;

	std::vector<Point> vertexes = { pos + Point(-halfWidth, -halfHeight),
									pos + Point( halfWidth, -halfHeight),
									pos + Point( halfWidth,  halfHeight),
									pos + Point(-halfWidth,  halfHeight) };
	nmUtils::rotate(vertexes, pos, angleDegree);

	return vertexes;
}
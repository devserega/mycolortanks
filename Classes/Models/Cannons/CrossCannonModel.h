#pragma once
#ifndef _CROSS_CANNON_MODEL_H_
#define _CROSS_CANNON_MODEL_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "CannonModel.h"
//----------------------------------------------------------------------------------------------------
class CrossCannonModel;
typedef std::shared_ptr<CrossCannonModel> CrossCannonModelRef;
//----------------------------------------------------------------------------------------------------
class CrossCannonModel
	: public CannonModel
{
	public:
		CrossCannonModel();
		virtual ~CrossCannonModel();
		virtual void update(float dt) override;
};
//----------------------------------------------------------------------------------------------------
#endif // _CROSS_CANNON_MODEL_H_
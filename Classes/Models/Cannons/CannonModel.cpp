#include "CannonModel.h"
#include "Models/WorldModel.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
CannonModel::CannonModel(Type type)
	: BaseModel(BaseModel::Type::CANNON)
	, mFireState(FireState::NONE)
	, mCurrReloadTimeSec(0.0f)
	, mMaxReloadTimeSec(0.0f)
	, mType(type)
{
}

CannonModel::~CannonModel() {
	mCannonModelListeners.clear();
}

void CannonModel::addCannonModelListener(ICannonModelListener* listener) {
	if (!listener) return;

	std::vector<ICannonModelListener*>::iterator it;
	it = std::find(mCannonModelListeners.begin(), mCannonModelListeners.end(), listener);
	if (it == mCannonModelListeners.end())
		mCannonModelListeners.push_back(listener);
}

void CannonModel::removeCannonModelListener(ICannonModelListener* listener) {
	if (!listener) return;

	std::vector<ICannonModelListener*>::iterator it;
	it = std::find(mCannonModelListeners.begin(), mCannonModelListeners.end(), listener);
	if (it != mCannonModelListeners.end())
		*it = nullptr;
}

void CannonModel::setRotation(float angle) {
	mRotationAngleDegree = angle;
	std::vector<ICannonModelListener*>::iterator it = mCannonModelListeners.begin();
	while (it != mCannonModelListeners.end()){
		ICannonModelListener* listener = *it;
		if (listener){
			listener->onCannonRotated(angle);
			it++;
		}
		else 
			it = mCannonModelListeners.erase(it);
	}
}

void CannonModel::fire(FireState state) {
	mFireState = state;
}

Vec2 CannonModel::getBulletSpawnPoint(size_t index) {
	size_t sz = mBulletsSpawnPoints.size();
	return mBulletsSpawnPoints[index % sz];
}
#pragma once
#ifndef _SINGLE_CANNON_MODEL_H_
#define _SINGLE_CANNON_MODEL_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "CannonModel.h"
//----------------------------------------------------------------------------------------------------
class SingleCannonModel;
typedef std::shared_ptr<SingleCannonModel> SingleCannonModelRef;
//----------------------------------------------------------------------------------------------------
class SingleCannonModel
	: public CannonModel
{
	public:
		SingleCannonModel();
		virtual ~SingleCannonModel();
		virtual void update(float dt) override;

	protected:
		std::weak_ptr<BulletModel> mLastFiredBullet;
};
//----------------------------------------------------------------------------------------------------
#endif // _SINGLE_CANNON_MODEL_H_
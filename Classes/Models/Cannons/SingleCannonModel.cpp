#include "SingleCannonModel.h"
#include "Models/WorldModel.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
SingleCannonModel::SingleCannonModel() 
	: CannonModel(CannonModel::Type::SINGLE)
{
}

SingleCannonModel::~SingleCannonModel() {
}

void SingleCannonModel::update(float dt) {
	std::vector<BulletModelRef> bullets;

	if (mFireState == FireState::ACTIVE && mLastFiredBullet.expired()) {
		mCurrReloadTimeSec = mMaxReloadTimeSec;
		auto bullet = std::make_shared<BulletModel>();
		bullet->setRotation(mRotationAngleDegree);
		bullet->setMoveSpeed(mStartBulletSpeed);
		bullet->setWorldModel(mWorldModel);
		bullet->addBaseModelListener(mWorldModel.get());
		bullet->setOwner(mOwner);
		bullet->setLifetime(0.5f);
		bullet->setDamage(2.0f);
		bullet->setTeamIndex(mTeamIndex);

		Vec2 startBulletPos = getPosition() + getBulletSpawnPoint(0);
		nmUtils::rotate(startBulletPos, mPosition, mRotationAngleDegree);
		bullet->setPosition(startBulletPos);

		mLastFiredBullet = bullet;

		bullets.push_back(bullet);
	}
	else if (mCurrReloadTimeSec > FLT_MIN) {
		mCurrReloadTimeSec -= dt;
	}

	if (bullets.size()>0) {
		std::vector<ICannonModelListener*>::iterator it = mCannonModelListeners.begin();
		while (it != mCannonModelListeners.end())
		{
			ICannonModelListener* listener = *it;
			if (listener)
			{
				listener->onCannonFired(bullets);
				it++;
			}
			else it = mCannonModelListeners.erase(it);
		}
	}
}
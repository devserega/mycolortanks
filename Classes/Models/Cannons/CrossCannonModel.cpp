#include "CrossCannonModel.h"
#include "Models/WorldModel.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
CrossCannonModel::CrossCannonModel()
	: CannonModel(CannonModel::Type::CROSS)
{
}

CrossCannonModel::~CrossCannonModel() {
}

void CrossCannonModel::update(float dt) {
	std::vector<BulletModelRef> bullets;

	if (mFireState == FireState::ACTIVE && mCurrReloadTimeSec<FLT_MIN) {
		mCurrReloadTimeSec = mMaxReloadTimeSec;

		float angle = 0;
		for (size_t i = 0; i < 4;  ++i) {
			auto bullet = std::make_shared<BulletModel>();
			bullet->setRotation(mRotationAngleDegree + angle);
			bullet->setMoveSpeed(mStartBulletSpeed);
			bullet->setWorldModel(mWorldModel);
			bullet->addBaseModelListener(mWorldModel.get());
			bullet->setOwner(mOwner);
			bullet->setLifetime(0.5f);
			bullet->setDamage(1);
			bullet->setScale(0.5f);
			bullet->setTeamIndex(mTeamIndex);

			Vec2 startBulletPos = getPosition() + getBulletSpawnPoint(i);
			nmUtils::rotate(startBulletPos, mPosition, mRotationAngleDegree);
			bullet->setPosition(startBulletPos);

			angle += 90.0f;

			bullets.push_back(bullet);
		}
	}
	else if (mCurrReloadTimeSec > FLT_MIN) {
		mCurrReloadTimeSec -= dt;
	}

	if (bullets.size()>0) {
		std::vector<ICannonModelListener*>::iterator it = mCannonModelListeners.begin();
		while (it != mCannonModelListeners.end())
		{
			ICannonModelListener* listener = *it;
			if (listener)
			{
				listener->onCannonFired(bullets);
				it++;
			}
			else it = mCannonModelListeners.erase(it);
		}
	}
}
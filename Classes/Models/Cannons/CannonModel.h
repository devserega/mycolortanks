#pragma once
#ifndef _CANNON_MODEL_H_
#define _CANNON_MODEL_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "Models/BulletModel.h"
//----------------------------------------------------------------------------------------------------
class CannonModel;
typedef std::shared_ptr<CannonModel> CannonModelRef;
//----------------------------------------------------------------------------------------------------
class ICannonModelListener {
	public:
		virtual void onCannonRotated(float angle) = 0;
		virtual void onCannonFired(std::vector<BulletModelRef> bullets) = 0;
};
//----------------------------------------------------------------------------------------------------
class CannonModel 
	: public BaseModel 
{
	public:
		enum class FireState : size_t {
			ACTIVE = 0,
			STOP = 1,
			COUNT = 2,
			NONE
		};
		enum class Type : size_t {
			SINGLE = 0,
			DUAL = 1,
			CROSS = 2,
			COUNT = 3,
			NONE
		};
		CannonModel(Type type);
		virtual ~CannonModel();
		virtual Type getType() { return mType; }
		virtual void addCannonModelListener(ICannonModelListener* listener);
		virtual void removeCannonModelListener(ICannonModelListener* listener);
		virtual void setRotation(float angle);
		virtual void fire(FireState state);
		virtual void setStartBulletSpeed(float speed) { mStartBulletSpeed = speed; }
		virtual void setBulletsSpawnPoints(std::vector<cocos2d::Vec2>& points) { mBulletsSpawnPoints = points; }
		virtual const std::vector<cocos2d::Vec2>& getBulletsSpawnPoints() { return mBulletsSpawnPoints; }
		virtual void setReloadTime(float reloadTimeSec) { mMaxReloadTimeSec = reloadTimeSec; }
		virtual cocos2d::Vec2 getBulletSpawnPoint(size_t index);

	protected:
		std::vector<cocos2d::Vec2>			mBulletsSpawnPoints;
		float								mStartBulletSpeed;
		float								mRotationAngleDegree;
		Type								mType;
		std::vector<ICannonModelListener*>	mCannonModelListeners;
		FireState							mFireState;
		float								mMaxReloadTimeSec;
		float								mCurrReloadTimeSec;							
};
//----------------------------------------------------------------------------------------------------
#endif // _CANNON_MODEL_H_
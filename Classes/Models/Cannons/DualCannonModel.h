#pragma once
#ifndef _DUAL_CANNON_MODEL_H_
#define _DUAL_CANNON_MODEL_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "CannonModel.h"
//----------------------------------------------------------------------------------------------------
class DualCannonModel;
typedef std::shared_ptr<DualCannonModel> DualCannonModelRef;
//----------------------------------------------------------------------------------------------------
class DualCannonModel
	: public CannonModel
{
	public:
		DualCannonModel();
		virtual ~DualCannonModel();
		virtual void update(float dt) override;

	protected:
		size_t mCurrCannon;
};
//----------------------------------------------------------------------------------------------------
#endif // _DUAL_CANNON_MODEL_H_
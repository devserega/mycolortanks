#include "DualCannonModel.h"
#include "Models/WorldModel.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
DualCannonModel::DualCannonModel()
	: CannonModel(CannonModel::Type::DUAL)
	, mCurrCannon(0)
{
}

DualCannonModel::~DualCannonModel() {
}

void DualCannonModel::update(float dt) {
	std::vector<BulletModelRef> bullets;

	if (mFireState == FireState::ACTIVE &&  mCurrReloadTimeSec < FLT_MIN) {
		mCurrReloadTimeSec = mMaxReloadTimeSec;
		auto bullet = std::make_shared<BulletModel>();
		bullet->setRotation(mRotationAngleDegree);
		bullet->setMoveSpeed(mStartBulletSpeed);
		bullet->setWorldModel(mWorldModel);
		bullet->addBaseModelListener(mWorldModel.get());
		bullet->setOwner(mOwner);
		bullet->setLifetime(0.5f);
		bullet->setScale(0.75f);
		bullet->setDamage(4);
		bullet->setTeamIndex(mTeamIndex);

		Vec2 startBulletPos = getPosition() + getBulletSpawnPoint(mCurrCannon++ % 2);
		nmUtils::rotate(startBulletPos, mPosition, mRotationAngleDegree);
		bullet->setPosition(startBulletPos);

		bullets.push_back(bullet);
	}
	else if (mCurrReloadTimeSec > FLT_MIN) {
		mCurrReloadTimeSec -= dt;
	}

	if (bullets.size() > 0) {
		std::vector<ICannonModelListener*>::iterator it = mCannonModelListeners.begin();
		while (it != mCannonModelListeners.end())
		{
			ICannonModelListener* listener = *it;
			if (listener)
			{
				listener->onCannonFired(bullets);
				it++;
			}
			else it = mCannonModelListeners.erase(it);
		}
	}
}
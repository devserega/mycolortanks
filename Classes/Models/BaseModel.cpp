#include "BaseModel.h"
#include "WorldModel.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
static size_t getNextId() {
	static size_t nId = 0;
	return (++nId);
}

BaseModel::BaseModel(Type type)
	: mRotationAngleDegree(0.0f)
	, mPosition(0.0f, 0.0f)
	, mID(getNextId())
	, mDestroyed(false)
	, mType(type)
	, mOwner(nullptr)
	, mTeamIndex(0)
{
	mBaseModelListeners.clear();
}

BaseModel::~BaseModel() {
}

std::vector<Point> BaseModel::getBoundingBox(const Point& pos, float angleDegree) {
	std::vector<Point> result;
	return result;
}

void BaseModel::addBaseModelListener(BaseModelListener* listener) {
	if (!listener) return;

	std::vector<BaseModelListener*>::iterator it;
	it = std::find(mBaseModelListeners.begin(), mBaseModelListeners.end(), listener);
	if (it == mBaseModelListeners.end())
		mBaseModelListeners.push_back(listener);
}

void BaseModel::removeBaseModelListener(BaseModelListener* listener) {
	if (!listener) return;

	std::vector<BaseModelListener*>::iterator it;
	it = std::find(mBaseModelListeners.begin(), mBaseModelListeners.end(), listener);
	if (it != mBaseModelListeners.end())
		*it = nullptr;
}

bool BaseModel::linesIntersection(const Vec2& p10, const Vec2& p11, const Vec2& p20, const Vec2& p21) {
	auto area = [](const Vec2& a, const Vec2& b, const Vec2& c) {
		return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
	};

	auto intersect_1 = [](int a, int b, int c, int d) {
		if (a > b)  std::swap(a, b);
		if (c > d)  std::swap(c, d);
		return std::max(a, c) <= std::min(b, d);
	};

	return intersect_1(p10.x, p11.x, p20.x, p21.x)
		&& intersect_1(p10.y, p11.y, p20.y, p21.y)
		&& area(p10, p11, p20) * area(p10, p11, p21) <= 0
		&& area(p20, p21, p10) * area(p20, p21, p11) <= 0;

	return false;
}

bool BaseModel::pointInsideRectangle(const Vec2& xy1, const Vec2& xy2, const Vec2& point){
	if (point.x > xy1.x && point.x < xy2.x && point.y > xy1.y && point.y < xy2.y)
		return true;

	return false;
}


bool BaseModel::checkCollisionWithWorldBorders(const Point& pos) {
	if (mWorldModel) {
		auto worldBorders = mWorldModel->getWorldBorders();
		if (!worldBorders.containsPoint(pos+ worldBorders.origin)) {
			return true;
		}
	}

	return false;
}

float BaseModel::calculateAngles(float &startAngle, float dstAngle) {
	float diffAngle = 0.0f;

	if (startAngle > 0) {
		startAngle = fmodf(startAngle, 360.0f);
	}
	else {
		startAngle = fmodf(startAngle, -360.0f);
	}

	diffAngle = dstAngle - startAngle;
	if (diffAngle > 180) {
		diffAngle -= 360;
	}
	if (diffAngle < -180) {
		diffAngle += 360;
	}

	return diffAngle;
}

bool BaseModel::checkCollisionWithOtherModel(const Point& pos, float rotationAngleDegree, const BaseModelRef targetModel) {
	auto vertexes1 = getBoundingBox(pos, rotationAngleDegree);
	auto vertexes2 = targetModel->getBoundingBox(targetModel->getPosition(), targetModel->getRotation());

	if (vertexes1.size() == 4 && vertexes2.size() == 4) {
		if (pointInsideRectangle(vertexes1[0], vertexes1[2], targetModel->getPosition()))
			return true;

		if (pointInsideRectangle(vertexes2[0], vertexes2[2], getPosition()))
			return true;
	}

	if (vertexes1.size() > 1 && vertexes2.size() > 1) {
		for (size_t i = 0; i < vertexes1.size(); i++) {
			size_t nextI = (i + 1) % vertexes1.size();
			for (size_t j = 0; j < vertexes2.size(); j++) {
				size_t nextJ = (j + 1) % vertexes2.size();
				if (linesIntersection(vertexes1[i], vertexes1[nextI], vertexes2[j], vertexes2[nextJ])) {
					return true;
				}
			}
		}
	}

	return false;
}

void BaseModel::destroy() {
	mDestroyed = true;

	std::vector<BaseModelListener*>::iterator it = mBaseModelListeners.begin();
	while (it != mBaseModelListeners.end())
	{
		BaseModelListener* listener = *it;
		if (listener)
		{
			listener->onDestroyed(this);
			it++;
		}
		else it = mBaseModelListeners.erase(it);
	}
}

void BaseModel::setOwner(BaseModelRef owner) { 
	mOwner = owner; 
}

BaseModelRef BaseModel::getOwner() { 
	return mOwner; 
}
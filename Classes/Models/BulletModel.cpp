#include "BulletModel.h"
#include "WorldModel.h"

using namespace cocos2d;

static int colorIndex = 0;
BulletModel::BulletModel()
	: BaseModel(BaseModel::Type::BULLET)
	, mLifetime(0.0f)
	, mScale(1.0f)
{
	std::vector<Color3B> colors = {
		Color3B(255, 0, 0),
		Color3B(0, 255, 0),
		Color3B(30, 144, 255),
		Color3B(255, 255, 0),
		Color3B(255, 165, 0)
	};
	mColor = colors[colorIndex++ % colors.size()];
}

BulletModel::~BulletModel() {
}

void BulletModel::update(float dt) {
	size_t collisionCounter = 0;

	if (mLifetime > FLT_MIN) {
		mLifetime -= dt;
	}

	Vec2 newPosition = mPosition + Vec2(mMoveSpeed * dt, 0);
	nmUtils::rotate(newPosition, mPosition, mRotationAngleDegree);

	// check collsions with world borders
	if (!checkCollisionWithWorldBorders(newPosition) && mLifetime > FLT_MIN) {
		// check collisons with other models
		auto modelList = mWorldModel->getModelsList();
		for (size_t j = 0; j < modelList.size(); ++j) {
			BaseModelRef model = modelList[j];

			if (!model)
				continue;

			if (model.get() != this && this->getOwner() != model) {
				bool isCollision = checkCollisionWithOtherModel(newPosition, mRotationAngleDegree, model);
				if (isCollision) {
					mWorldModel->onCollisionDetected(this, model.get());
				}
			}
		}

		if (collisionCounter == 0) {
			setPosition(newPosition);
		}
	}
	else{
		destroy();
	}
}

std::vector<Point> BulletModel::getBoundingBox(const Point& pos, float angleDegree) {
	float halfWidth = mBoundingBoxSize.width*mScale / 2;
	float halfHeight = mBoundingBoxSize.height*mScale / 2;

	std::vector<Point> vertexes = { pos + Point(-halfWidth, -halfHeight),
									pos + Point( halfWidth, -halfHeight),
									pos + Point( halfWidth,  halfHeight),
									pos + Point(-halfWidth,  halfHeight) };
	nmUtils::rotate(vertexes, pos, angleDegree);

	return vertexes;
}

void BulletModel::setPosition(const Vec2& newPosition) {
	Vec2 deltaMove = newPosition - mPosition;
	BaseModel::setPosition(newPosition);

	std::vector<BaseModelListener*>::iterator it = mBaseModelListeners.begin();
	while (it != mBaseModelListeners.end())
	{
		BaseModelListener* listener = *it;
		if (listener)
		{
			listener->onMoved(this, deltaMove);

			it++;
		}
		else it = mBaseModelListeners.erase(it);
	}
}
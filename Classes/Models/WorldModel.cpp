#include "WorldModel.h"
#include "BaseModel.h"
#include "Controllers/AI/AIController.h"
//----------------------------------------------------------------------------------------------------
using namespace std;
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
WorldModel::WorldModel() {
}

WorldModel::~WorldModel() {
	mWorldModelListeners.clear();
}

void WorldModel::update(float dt) {
	mCollisions.clear();

	for (size_t i = 0; i < mAIControllers.size(); ++i) {
		AIControllerRef controller = mAIControllers[i];
		if (controller) {
			controller->update(dt);
		}
	}

	for (size_t i = 0; i < mModelsList.size(); ++i) {
		BaseModelRef model = mModelsList[i];
		if (model) {
			model->update(dt);
		}
	}

	// process collisions
	for(auto& collisionPair : mCollisions) {
		BaseModel* model1 = std::get<0>(collisionPair.second);
		BaseModel* model2 = std::get<1>(collisionPair.second);
		if (	model1->getType() == BaseModel::Type::BULLET &&
				model2->getType() == BaseModel::Type::TANK && 
				model1->getTeamIndex() != model2->getTeamIndex())
		{
			// make damage to tank armor and mark bullet as destroyed 
			TankModel* tank = dynamic_cast<TankModel*>(model2);
			BulletModel* bullet = dynamic_cast<BulletModel*>(model1);
			bullet->destroy();
			tank->damage(bullet->getDamage());
		}
		else if (	model2->getType() == BaseModel::Type::BULLET &&
					model1->getType() == BaseModel::Type::TANK &&
					model1->getTeamIndex() != model2->getTeamIndex())
		{
			// make hit to tank armor and mark bullet as destroyed 
			TankModel* tank = dynamic_cast<TankModel*>(model1);
			BulletModel* bullet = dynamic_cast<BulletModel*>(model2);
			bullet->destroy();
			tank->damage(bullet->getDamage());
		}
	}

	// cleanup destroyed objects
	std::vector<BaseModelRef>::iterator it = mModelsList.begin();
	while (it != mModelsList.end())
	{
		BaseModelRef model = *it;
		if (model && !model->isDestroyed())
			it++;
		else 
			it = mModelsList.erase(it);
	}
}

void WorldModel::addModel(BaseModelRef model) {
	if (!model)
		return;

	model->addBaseModelListener(this);
	mModelsList.push_back(model);
}

void WorldModel::removeModel(BaseModel* model) {
	if (!model)
		return;

	auto foundIt = std::remove_if(mModelsList.begin(), mModelsList.end(),
		[model](const BaseModelRef tmp){
			return tmp.get() == model;
		});

	mModelsList.erase(foundIt, mModelsList.end());
}

void WorldModel::removeModel(BaseModelRef model) {
	if (!model)
		return;

	removeModel(model.get());
}

void WorldModel::addController(AIControllerRef aiController) {
	if (!aiController)
		return;

	mAIControllers.push_back(aiController);
}

void WorldModel::onDestroyed(BaseModel* model) {
	//removeModel(model);

	auto tankModel = dynamic_cast<TankModel*>(model);
	if (tankModel) {
		// tank destroyed remove AI controller
		auto foundIt = std::remove_if(mAIControllers.begin(), mAIControllers.end(),
			[model](const AIControllerRef tmp) {
				return tmp->getTankModel().get() == model;
			});

		mAIControllers.erase(foundIt, mAIControllers.end());
	}

	// send event 
	std::vector<IWorldModelListener*>::iterator it = mWorldModelListeners.begin();
	while (it != mWorldModelListeners.end()){
		IWorldModelListener* listener = *it;
		if (listener){
			listener->onDestroyed(model);
			it++;
		}
		else it = mWorldModelListeners.erase(it);
	}
}

void WorldModel::onMoved(BaseModel* model, const Vec2& deltaMove) {
}

void WorldModel::onCollisionDetected(BaseModel* model1, BaseModel* model2) {
	unsigned long long collisionCode = model1->getID()*model2->getID();
	auto foundIt = mCollisions.find(collisionCode);
	if (foundIt == mCollisions.end()) {
		mCollisions[collisionCode] = std::make_tuple(model1, model2);
	}
}

TankModelRef WorldModel::findPlayerTank() {
	for (size_t i = 0; i < mModelsList.size(); ++i) {
		BaseModelRef model = mModelsList[i];
		if (model) {
			TankModelRef tankModel = std::dynamic_pointer_cast<TankModel>(model);
			if (tankModel && tankModel->isPlayerTank()) {
				return tankModel;
			}
		}
	}

	return nullptr;
}

void WorldModel::addWorldModelListener(IWorldModelListener* listener) {
	if (!listener) return;

	std::vector<IWorldModelListener*>::iterator it;
	it = std::find(mWorldModelListeners.begin(), mWorldModelListeners.end(), listener);
	if (it == mWorldModelListeners.end())
		mWorldModelListeners.push_back(listener);
}

void WorldModel::removeWorldModelListener(IWorldModelListener* listener) {
	if (!listener) return;

	std::vector<IWorldModelListener*>::iterator it;
	it = std::find(mWorldModelListeners.begin(), mWorldModelListeners.end(), listener);
	if (it != mWorldModelListeners.end())
		*it = nullptr;
}
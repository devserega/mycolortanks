#pragma once
#ifndef _PLATFORM_MODEL_H_
#define _PLATFORM_MODEL_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "utilities.h"
#include "BaseModel.h"
//----------------------------------------------------------------------------------------------------
class TankModel;
class PlatformModel;
typedef std::shared_ptr<PlatformModel> PlatformModelRef;
//----------------------------------------------------------------------------------------------------
class IPlatformModelListener {
public:
	virtual void onPlatformRotated(float angle) = 0;
	virtual void onPlatformMoved(const cocos2d::Vec2& deltaMove) = 0;
};
//----------------------------------------------------------------------------------------------------
class PlatformModel 
	: public BaseModel
{
	public:
		enum class Type : size_t {
			WHITE = 0,
			BLUE = 1,
			OLIVE = 2,
			BLACK = 3,
			YELLOW = 4,
			RED = 5,
			GREEN = 6,
			COUNT = 7,
			NONE
		};
		enum class MoveDirection : size_t {
			FORWARD = 0,
			COUNT = 1,
			NONE
		};
		enum class RotateDirection : size_t {
			LEFT = 0,
			RIGHT = 1,
			COUNT = 2,
			NONE
		};
		PlatformModel();
		virtual ~PlatformModel();
		virtual void update(float dt);
		virtual void setMoveSpeed(float speed) { mMoveSpeed = speed; }
		virtual float getMoveSpeed() {return mMoveSpeed;}
		virtual void setRotationSpeed(float rotationSpeed) { mRotationSpeed = rotationSpeed; }
		virtual float getRotationSpeed() { return mRotationSpeed; }
		virtual void setType(Type type) { mType = type; }
		virtual Type getType() { return mType; }
		virtual void addPlatformModelListener(IPlatformModelListener* listener);
		virtual void removePlatformModelListener(IPlatformModelListener* listener);
		virtual void setRotation(float angle);
		virtual void setPosition(const cocos2d::Vec2& pos);
		virtual void move(MoveDirection dir);
		virtual void rotate(float targetAngleDegree);
		virtual std::vector<cocos2d::Point> getBoundingBox(const cocos2d::Point& pos, float angleDegree);
		std::tuple<float, cocos2d::Vec2> getNextMovementAttributes(float dt);
		virtual MoveDirection getMoveDirection() { return mMoveDirection; }
		virtual RotateDirection getRotateDirection() { return mRotateDirection; }

	protected:
		float mMoveSpeed;
		float mRotationSpeed;
		Type mType;
		MoveDirection mMoveDirection;
		float mTargetAngleDegree;
		RotateDirection mRotateDirection;
		std::vector<IPlatformModelListener*> mPlatformModelListeners;

		std::tuple<float, cocos2d::Vec2> processMoveAction(float dt, float moveSpeed);
		std::tuple<float, cocos2d::Vec2> processRotateAction(float dt, float moverotationSpeedSpeed);
		std::tuple<float, cocos2d::Vec2> processAction(float dt, float targetAngleDegree, float moveSpeed);
};
//----------------------------------------------------------------------------------------------------
#endif // _PLATFORM_MODEL_H_
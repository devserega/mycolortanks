#pragma once
#ifndef _TANK_MODEL_H_
#define _TANK_MODEL_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "Models/TankModel.h"
#include "Models/PlatformModel.h"
#include "Models/Cannons/CannonModel.h"
#include "BaseModel.h"
//----------------------------------------------------------------------------------------------------
class TankModel;
typedef std::shared_ptr<TankModel> TankModelRef;
//----------------------------------------------------------------------------------------------------
class ITankModelListener {
	public:
		virtual void onSetPlatform(TankModel& tankModel) = 0;
		virtual void onSetCannon(TankModel& tankModel) = 0;
		virtual void onChangedArmor(TankModel& tankModel) = 0;
};
//----------------------------------------------------------------------------------------------------
class TankModel 
	: public BaseModel {
	public:
		TankModel();
		virtual ~TankModel();
		virtual void update(float dt) override;
		virtual void setPlatform(PlatformModelRef platformModel);
		virtual void setCannon(CannonModelRef cannonModel);
		virtual void addTankModelListener(ITankModelListener* listener);
		virtual void removeTankModelListener(ITankModelListener* listener);
		virtual PlatformModelRef getPlatformModel() { return mPlatformModel; }
		virtual CannonModelRef getCannonModel() { return mCannonModel; }
		virtual void setWorldModel(WorldModelRef worldModel) override;
		std::vector<cocos2d::Point> getBoundingBox(const cocos2d::Point& pos, float angleDegree) override;
		const cocos2d::Vec2& getPosition() override;
		float getRotation() override;
		void setBounty(size_t bounty) { mBounty = bounty; }
		size_t getBounty() { return mBounty; }
		size_t getScore() { return mScore; }
		void setCurrArmor(int armor) { mCurrArmor = armor; };
		int getCurrArmor() { return mCurrArmor; }
		void setMaxArmor(int armor) { mMaxArmor = armor; };
		int getMaxArmor() { return mMaxArmor; }
		void setIsPlayerTank(bool isPlayerTank) { mIsPlayerTank = isPlayerTank; }
		bool isPlayerTank() { return mIsPlayerTank; }
		const cocos2d::Color3B& getColor() { return mTankColor; }
		void setColor(cocos2d::Color3B& color) { mTankColor = color; }
		void damage(int damage);
		void setPosition(const cocos2d::Vec2& pos) override;

	protected:
		PlatformModelRef					mPlatformModel;
		CannonModelRef						mCannonModel;
		std::vector<ITankModelListener*>	mTankModelListeners;
		int									mCurrArmor;
		int									mMaxArmor;
		bool								mModelUpdated;
		size_t								mBounty;
		size_t								mScore;
		bool								mIsPlayerTank;
		cocos2d::Color3B					mTankColor;
};
//----------------------------------------------------------------------------------------------------
#endif // _TANK_MODEL_H_
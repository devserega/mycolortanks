#pragma once
#ifndef _WORLD_MODEL_H_
#define _WORLD_MODEL_H_
//------------------------------------------------------------------------------------------------- ---
#include "cocos2d.h"
#include "Models/TankModel.h"
#include "Controllers/AI/AIController.h"
#include <memory>
//----------------------------------------------------------------------------------------------------
class WorldModel;
typedef std::shared_ptr<WorldModel> WorldModelRef;
//----------------------------------------------------------------------------------------------------
class IWorldModelListener {
	public:
		virtual void onDestroyed(BaseModel* model) = 0;
};
//----------------------------------------------------------------------------------------------------
class WorldModel 
	: public BaseModelListener
{
	public:
		WorldModel();
		virtual ~WorldModel();
		void update(float dt);
		void addModel(BaseModelRef model);
		void removeModel(BaseModelRef model);
		void removeModel(BaseModel* model);
		void addController(AIControllerRef aiController);
		const cocos2d::Rect& getWorldBorders() { return mWorldBorders; }
		void setWorldBorders(const cocos2d::Rect& worldBorders) { mWorldBorders = worldBorders; }
		const std::vector<BaseModelRef>& getModelsList() { return mModelsList; }
		TankModelRef findPlayerTank();
		virtual void addWorldModelListener(IWorldModelListener* listener);
		virtual void removeWorldModelListener(IWorldModelListener* listener);

		/* BaseModelListener */
		void onDestroyed(BaseModel* model) override;
		void onMoved(BaseModel* model, const cocos2d::Vec2& deltaMove) override;
		void onCollisionDetected(BaseModel* model1, BaseModel* model2) override;

	protected:
		std::vector<BaseModelRef>			mModelsList;
		cocos2d::Rect						mWorldBorders;
		BaseModel::TCollisions				mCollisions;
		std::vector<AIControllerRef>		mAIControllers;
		std::vector<IWorldModelListener*>	mWorldModelListeners;
};
//----------------------------------------------------------------------------------------------------
#endif // _WORLD_MODEL_H_
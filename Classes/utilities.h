#pragma once
#ifndef _UTILITIES_H_
#define _UTILITIES_H_
//----------------------------------------------------------------------------------------------------
#include <vector>
#include <functional>
#include <sstream>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
//----------------------------------------------------------------------------------------------------
#define SECTION_SEGMENTS 10
#define SPLINE_TENSION 0.01f
#define TAN_60 1.732050
//----------------------------------------------------------------------------------------------------
#ifdef NM_DEBUG
#define NMAssert(cond, msg, ...) \
	do { if (!(cond)) { cocos2d::log(msg, __VA_ARGS__); \
		CC_ASSERT(cond);} } while(0)
#else
#define NMAssert(cond, msg, ...) \
	do { if (!(cond)) { cocos2d::log(msg, __VA_ARGS__); } while(0)
#endif // NM_DEBUG

#define NMAssertRet(cond, ret, msg, ...) \
	do { \
		if (!(cond)) \
		{ \
			NMAssert(false, msg, ##__VA_ARGS__); \
			return (ret); \
		} \
	} while(0)

#define NMAssertRetVoid(cond, msg, ...) \
	do { \
		if (!(cond)) \
		{ \
			NMAssert(false, msg, ##__VA_ARGS__); \
			return; \
		} \
	} while(0)

#define DEG_TO_RAD(degree) degree * (3.141592653589/180)
#define RAD_TO_DEG(rad) rad * 57.2958f 

//----------------------------------------------------------------------------------------------------
// init button macros
// check button and asign click event func
// for using in init func of your layer
#define INIT_BUTTON(btnName, onClickFunc, rootNode) \
	{ ui::Button* childBtn = nmUtils::findChildNode<ui::Button>(rootNode, btnName); \
	NMAssert(childBtn != nullptr, "File: %s, Line: %d, - " btnName " not found", __FILE__, __LINE__); \
	if (!childBtn) return false; \
	childBtn->addClickEventListener(CC_CALLBACK_1(onClickFunc, this)); }

#define INIT_CHECKBOX_AS_MEMBER(checkBoxCtrl, checkBoxName, onClickFunc, rootNode) \
	{ checkBoxCtrl = nmUtils::findChildNode<ui::CheckBox>(rootNode, checkBoxName); \
	NMAssert(checkBoxCtrl != nullptr, "File: %s, Line: %d, - " checkBoxName " not found", __FILE__, __LINE__); \
	if (!checkBoxCtrl) return false; \
	CC_SAFE_RETAIN(checkBoxCtrl); \
	checkBoxCtrl->addEventListener(CC_CALLBACK_2(onClickFunc, this)); }

#define INIT_BUTTON_AS_MEMBER(btnControl, btnName, onClickFunc, rootNode) \
	{ btnControl = nmUtils::findChildNode<ui::Button>(rootNode, btnName); \
	NMAssert(btnControl != nullptr, "File: %s, Line: %d, - " btnName " not found", __FILE__, __LINE__); \
	if (!btnControl) return false; \
	CC_SAFE_RETAIN(btnControl); \
	btnControl->addClickEventListener(CC_CALLBACK_1(onClickFunc, this)); }

#define INIT_CONTROL_AS_MEMBER(control, controlName, controlType, rootNode) \
	{ control = nmUtils::findChildNode<controlType>(rootNode, controlName); \
	NMAssert(control != nullptr, std::string("File: %s, Line: %d, - " + std::string(controlName) + " not found").c_str(), __FILE__, __LINE__); \
	if (!control) return false; \
	CC_SAFE_RETAIN(control); }

#define INIT_REF_CONTROL_AS_MEMBER(control, controlName, controlType, rootNode) \
	{ control.reset(); /*����� reset ����� �� ��������� ������� ���������*/ \
	control = RefPtr<controlType>(nmUtils::findChildNode<controlType>(rootNode, controlName)); \
	NMAssert(control != nullptr, std::string("File: %s, Line: %d, - " + std::string(controlName) + " not found").c_str(), __FILE__, __LINE__); \
	if (!control) return false; }

#define INIT_REF_BUTTON_AS_MEMBER(btnControl, btnName, onClickFunc, rootNode) \
	{ btnControl.reset(); /*����� reset ����� �� ��������� ������� ���������*/ \
	btnControl = RefPtr<ui::Button>(nmUtils::findChildNode<ui::Button>(rootNode, btnName)); \
	NMAssert(btnControl != nullptr, "File: %s, Line: %d, - " btnName " not found", __FILE__, __LINE__); \
	if (!btnControl) return false; \
	btnControl->addClickEventListener(CC_CALLBACK_1(onClickFunc, this)); }

//----------------------------------------------------------------------------------------------------
namespace nmUtils
{
	template<class T>
	const T& clamp(const T& val, const T& low, const T& high)
	{
		return std::max(std::min(val, high), low);
	}

	template<class T>
	T* findChildNode(cocos2d::Node* pParentNode, const char * pName)
	{
		if (pParentNode == nullptr || pName == nullptr || pName[0] == '\0')
			return nullptr;

		std::hash<std::string> hashFunc;
		size_t nameHash = hashFunc(pName);
		cocos2d::Node* pResult = nullptr;

		auto childNodeFunc = [&hashFunc, &nameHash, pName, &pResult](cocos2d::Node * pNode)
		{
			if (!pNode)
				return false;

			size_t hash = hashFunc(pNode->getName());
			if (nameHash == hash && pNode->getName().compare(pName) == 0)
			{
				pResult = pNode;
				return true;
			}
			else
				return false;
		};

		pParentNode->enumerateChildren(std::string("//") + pName, childNodeFunc);
		return dynamic_cast<T*>(pResult);
	}

	template<class T>
	T* findChildNode(cocos2d::Node* pParentNode, const std::string & strName)
	{
		if (strName.empty())
			return nullptr;

		return findChildNode<T>(pParentNode, strName.c_str());
	}

	template<class T>
	T stringTo(const std::string & str)
	{
		std::istringstream iss(str);
		T nResult = 0;
		iss >> nResult;
		return nResult;
	}

	void rotate(cocos2d::Point& vertex, const cocos2d::Point& pivot, float angleDegree);
	void rotate(std::vector<cocos2d::Point>& vertexes, const cocos2d::Point& pivot, float angleDegree);
}
//----------------------------------------------------------------------------------------------------
#endif // _UTILITIES_H_
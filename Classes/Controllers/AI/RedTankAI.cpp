#include "RedTankAI.h"
#include "Models/WorldModel.h"
#include "PursuitCommand.h"
#include "DistanceAttackCommand.h"
#include "FleeCommand.h"
#include "common.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
RedTankAIRef RedTankAI::create(TankModelRef tankModel) {
	RedTankAIRef pRet = std::make_shared<RedTankAI>(tankModel);
	if (pRet && pRet->init()) {
		return pRet;
	}
	else {
		return nullptr;
	}
}

bool RedTankAI::init() {
	BaseModelRef baseModel = std::dynamic_pointer_cast<BaseModel>(mTankModel);
	baseModel->addBaseModelListener(this);

	return true;
}

RedTankAI::RedTankAI(TankModelRef tankModel) 
	: AIController(tankModel)
	, mMode(ModeType::NONE)
	, mLastDistance(0.0f)
{
	auto worldModel = mTankModel->getWorldModel();
	if (worldModel) {
		auto playerTank = worldModel->findPlayerTank();
		if (playerTank) {
			auto playerTankPos = playerTank->getPosition();
			auto selfPos = mTankModel->getPosition();
			mLastDistance = sqrt(pow(playerTankPos.x - selfPos.x, 2) + pow(playerTankPos.y - selfPos.y, 2));
		}
	}
}

RedTankAI::~RedTankAI() {
}

void RedTankAI::onDestroyed(BaseModel* model) {
	BaseModelRef baseModel = std::dynamic_pointer_cast<BaseModel>(mTankModel);
	baseModel->removeBaseModelListener(this);
}

void RedTankAI::logic() {
	TankModelRef playerTankModel = nullptr;
	float newDistance = 0.0f;
	auto worldModel = mTankModel->getWorldModel();
	if (worldModel) {
		auto playerTankModel = worldModel->findPlayerTank();
		if (playerTankModel) {
			auto playerTankPos = playerTankModel->getPosition();
			auto selfPos = mTankModel->getPosition();
			newDistance = sqrt(pow(playerTankPos.x - selfPos.x, 2) + pow(playerTankPos.y - selfPos.y, 2));
		}
	}

	if (newDistance >= mLastDistance) {
		auto pursuit = std::make_shared<PursuitCommand>(mTankModel, 1.0f);
		pursuit->setStopDistance(STOP_DISTANCE);
		pursuit->setMoveSpeed(PLAYER_MOVE_SPEED / 1.2f);
		mCommands.push_back(pursuit);

		auto attack = std::make_shared<DistanceAttackCommand>(mTankModel, 1.0f);
		attack->setAttackDistance(1000.0f);
		mCommands.push_back(attack);
	}
	else {
		auto flee = std::make_shared<FleeCommand>(mTankModel, 1.0f);
		flee->setMoveSpeed(PLAYER_MOVE_SPEED * 1.2f);
		mCommands.push_back(flee);
	}
	mLastDistance = newDistance;
}
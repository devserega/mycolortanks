#include "GreenTankAI.h"
#include "Models/WorldModel.h"
#include "PursuitCommand.h"
#include "DistanceAttackCommand.h"
#include "FleeCommand.h"
#include "common.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
const float COMMAND_DURATION = 3.0f;
//----------------------------------------------------------------------------------------------------
GreenTankAIRef GreenTankAI::create(TankModelRef tankModel) {
	GreenTankAIRef pRet = std::make_shared<GreenTankAI>(tankModel);
	if (pRet && pRet->init()) {
		return pRet;
	}
	else {
		return nullptr;
	}
}

bool GreenTankAI::init() {
	BaseModelRef baseModel = std::dynamic_pointer_cast<BaseModel>(mTankModel);
	baseModel->addBaseModelListener(this);

	return true;
}

GreenTankAI::GreenTankAI(TankModelRef tankModel)
	: AIController(tankModel)
	, mMode(ModeType::NONE)
	, mDistanceToPlayer(0.0f)
{
	auto worldModel = mTankModel->getWorldModel();
	if (worldModel) {
		auto playerTank = worldModel->findPlayerTank();
		if (playerTank) {
			auto playerTankPos = playerTank->getPosition();
			auto aiTankPos = mTankModel->getPosition();
			mDistanceToPlayer = sqrt(pow(playerTankPos.x - aiTankPos.x, 2) + pow(playerTankPos.y - aiTankPos.y, 2));
		}
	}
}

GreenTankAI::~GreenTankAI() {
}

void GreenTankAI::onDestroyed(BaseModel* model) {
	BaseModelRef baseModel = std::dynamic_pointer_cast<BaseModel>(mTankModel);
	baseModel->removeBaseModelListener(this);
}

void GreenTankAI::logic() {
	TankModelRef playerTankModel = nullptr;
	float newDistance = 0.0f;
	auto worldModel = mTankModel->getWorldModel();
	if (worldModel) {
		auto playerTankModel = worldModel->findPlayerTank();
		if (playerTankModel) {
			auto playerTankPos = playerTankModel->getPosition();
			auto selfPos = mTankModel->getPosition();
			newDistance = sqrt(pow(playerTankPos.x - selfPos.x, 2) + pow(playerTankPos.y - selfPos.y, 2));
		}
	}

	auto pursuit = std::make_shared<PursuitCommand>(mTankModel, COMMAND_DURATION);
	pursuit->setStopDistance(STOP_DISTANCE);
	pursuit->setMoveSpeed(PLAYER_MOVE_SPEED / 1.4f);
	mCommands.push_back(pursuit);

	auto attack = std::make_shared<DistanceAttackCommand>(mTankModel, COMMAND_DURATION);
	const Size visibleSize = Director::getInstance()->getVisibleSize();
	float attackDistance = sqrt(pow(visibleSize.width/2, 2) + pow(visibleSize.height/2, 2)) / 3.0f;
	attack->setAttackDistance(attackDistance);
	mCommands.push_back(attack);

	mDistanceToPlayer = newDistance;
}
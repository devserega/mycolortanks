#include "Controllers/AI/DistanceAttackCommand.h"
#include "Models/TankModel.h"
#include "Models/WorldModel.h"

DistanceAttackCommand::DistanceAttackCommand(TankModelRef aiTankModel, float duration)
	: BaseCommand(aiTankModel, duration)
	, mAttackDistance(0.0f)
{
}

DistanceAttackCommand::~DistanceAttackCommand() {
	if (!mAITankModel)
		return;

	auto cannonModel = mAITankModel->getCannonModel();
	if (!cannonModel)
		return;

	cannonModel->fire(CannonModel::FireState::NONE);
}

bool DistanceAttackCommand::execute(float dt) {
	if (BaseCommand::execute(dt)) {
		auto worldModel = mAITankModel->getWorldModel();
		if (worldModel) {
			auto aiTankPos = mAITankModel->getPosition();
			auto cannonModel = mAITankModel->getCannonModel();
			auto playerTank = worldModel->findPlayerTank();

			if (!playerTank && cannonModel) {
				cannonModel->fire(CannonModel::FireState::NONE);
			}
			else {
				auto playerTankPos = playerTank->getPosition();
				float angleRadians = atan2(playerTankPos.y - aiTankPos.y, playerTankPos.x - aiTankPos.x);

				if (cannonModel) {
					float angleDegree = RAD_TO_DEG(angleRadians);
					cannonModel->setRotation(angleDegree);
				}

				auto distance = sqrt(pow(playerTankPos.x - aiTankPos.x, 2) + pow(playerTankPos.y - aiTankPos.y, 2));
				if (distance <= mAttackDistance) {
					cannonModel->fire(CannonModel::FireState::ACTIVE);
				}
				else {
					cannonModel->fire(CannonModel::FireState::NONE);
				}
			}
		}
	}

	return true;
}
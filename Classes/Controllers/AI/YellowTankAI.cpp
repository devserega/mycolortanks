#include "YellowTankAI.h"
#include "Models/WorldModel.h"
#include "PursuitCommand.h"
#include "DistanceAttackCommand.h"
#include "RandomMoveCommand.h"
#include "common.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
//----------------------------------------------------------------------------------------------------
YellowTankAIRef YellowTankAI::create(TankModelRef tankModel) {
	YellowTankAIRef pRet = std::make_shared<YellowTankAI>(tankModel);
	if (pRet && pRet->init()) {
		return pRet;
	}
	else {
		return nullptr;
	}
}

bool YellowTankAI::init() {
	BaseModelRef baseModel = std::dynamic_pointer_cast<BaseModel>(mTankModel);
	baseModel->addBaseModelListener(this);

	return true;
}

YellowTankAI::YellowTankAI(TankModelRef tankModel)
	: AIController(tankModel)
{
}

YellowTankAI::~YellowTankAI() {
}

void YellowTankAI::onDestroyed(BaseModel* model) {
	BaseModelRef baseModel = std::dynamic_pointer_cast<BaseModel>(mTankModel);
	baseModel->removeBaseModelListener(this);
}

void YellowTankAI::logic() {
	auto randomMove = std::make_shared<RandomMoveCommand>(mTankModel, 5.0f);
	randomMove->setStopDistance(STOP_DISTANCE);
	randomMove->setMoveSpeed(PLAYER_MOVE_SPEED / 1.25f);
	mCommands.push_back(randomMove);

	auto pursuit = std::make_shared<PursuitCommand>(mTankModel, 2.0f);
	pursuit->setDelay(5.0f);
	pursuit->setStopDistance(STOP_DISTANCE);
	pursuit->setMoveSpeed(PLAYER_MOVE_SPEED / 1.25f);
	mCommands.push_back(pursuit);

	auto attack = std::make_shared<DistanceAttackCommand>(mTankModel, 2.0f);
	attack->setDelay(5.0f);
	attack->setAttackDistance(1000.0f);
	mCommands.push_back(attack);
}
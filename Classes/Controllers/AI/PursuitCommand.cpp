#include "Controllers/AI/PursuitCommand.h"
#include "Models/TankModel.h"
#include "Models/WorldModel.h"
#include "common.h"

PursuitCommand::PursuitCommand(TankModelRef selfTankModel, float duration)
	: BaseCommand(selfTankModel, duration)
	, mStopDistance(0.0f)
	, mPlatformAngleRadians(0.0f)
	, mPlatformSpeed(0.0f)
	, mInit(false)
{
}

PursuitCommand::~PursuitCommand() {
	mAITankModel->getPlatformModel()->move(PlatformModel::MoveDirection::NONE);
}

bool PursuitCommand::execute(float dt) {
	if (BaseCommand::execute(dt) && mAITankModel) {
		auto worldModel = mAITankModel->getWorldModel();
		if (!worldModel)
			return false;

		auto playerTank = worldModel->findPlayerTank();
		if (!playerTank)
			return false;

		auto aiPlatformModel = mAITankModel->getPlatformModel();
		if (!aiPlatformModel)
			return false;

		if (!mInit) {
			mInit = true;

			auto aiTankPos = mAITankModel->getPosition();
			auto playerTankPos = playerTank->getPosition();
			mPlatformAngleRadians = atan2(playerTankPos.y - aiTankPos.y, playerTankPos.x - aiTankPos.x);
		}

		auto aiTankPos = mAITankModel->getPosition();
		auto enemyTankPos = playerTank->getPosition();
		float angleRadians = atan2(enemyTankPos.y - aiTankPos.y, enemyTankPos.x - aiTankPos.x);
		aiPlatformModel->setMoveSpeed(mPlatformSpeed);

		auto distance = sqrt(pow(enemyTankPos.x - aiTankPos.x, 2) + pow(enemyTankPos.y - aiTankPos.y, 2));
		if (distance <= mStopDistance) {
			aiPlatformModel->move(PlatformModel::MoveDirection::NONE);
		}
		else {
			float angleDegree = RAD_TO_DEG(mPlatformAngleRadians);
			aiPlatformModel->move(PlatformModel::MoveDirection::FORWARD);
			aiPlatformModel->rotate(angleDegree);
		}
	}

	return true;
}
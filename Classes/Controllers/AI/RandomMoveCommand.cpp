#include "Controllers/AI/RandomMoveCommand.h"
#include "Models/TankModel.h"
#include "Models/WorldModel.h"
#include "common.h"

RandomMoveCommand::RandomMoveCommand(TankModelRef selfTankModel, float duration)
	: BaseCommand(selfTankModel, duration)
	, mStopDistance(0.0f)
	, mPlatformSpeed(0.0f)
	, mPlatformAngleDegree(0.0f)
	, mInit(false)
{
}

RandomMoveCommand::~RandomMoveCommand() {
	mAITankModel->getPlatformModel()->move(PlatformModel::MoveDirection::NONE);
}

bool RandomMoveCommand::execute(float dt) {
	if (BaseCommand::execute(dt) && mAITankModel) {
		auto worldModel = mAITankModel->getWorldModel();
		if (!worldModel)
			return false;

		auto playerTank = worldModel->findPlayerTank();
		if (!playerTank)
			return false;

		auto platformModel = mAITankModel->getPlatformModel();

		if (!mInit) {
			mInit = true;

			mPlatformAngleDegree = cocos2d::RandomHelper::random_int(1, 360);
			platformModel->rotate(mPlatformAngleDegree);
		}

		auto aiTankPos = mAITankModel->getPosition();
		auto playerTankPos = playerTank->getPosition();

		platformModel->setMoveSpeed(mPlatformSpeed);

		auto distance = sqrt(pow(playerTankPos.x - aiTankPos.x, 2) + pow(playerTankPos.y - aiTankPos.y, 2));
		if (distance <= mStopDistance) {
			platformModel->move(PlatformModel::MoveDirection::NONE);
		}
		else {
			platformModel->move(PlatformModel::MoveDirection::FORWARD);		
		}
	}

	return true;
}
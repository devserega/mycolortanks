#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _AI_CONTROLLER_H_
#define _AI_CONTROLLER_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "Models/BaseModel.h"
#include "BaseCommand.h"
//----------------------------------------------------------------------------------------------------
class TankModel;
typedef std::shared_ptr<TankModel> TankModelRef;
class AIController;
typedef std::shared_ptr<AIController> AIControllerRef;
//----------------------------------------------------------------------------------------------------
class AIController 
	: public BaseModelListener
{
	public:
		AIController(TankModelRef tankModel) : mTankModel(tankModel){}
		virtual ~AIController() {}

		virtual void update(float dt) {
			if (mCommands.size() == 0) {
				logic();
			}
			else {
				for (size_t i = 0; i < mCommands.size(); ++i)
					mCommands[i]->execute(dt);

				std::vector<BaseCommandRef>::iterator it = mCommands.begin();
				while (it != mCommands.end())
				{
					BaseCommandRef command = *it;
					if (command && !command->isFinished())
						it++;
					else
						it = mCommands.erase(it);
				}
			}
		}

		virtual void logic() = 0;
		virtual TankModelRef getTankModel() { return mTankModel; }

	protected:
		TankModelRef				mTankModel;
		std::vector<BaseCommandRef>	mCommands;
};
//----------------------------------------------------------------------------------------------------
#endif // _AI_CONTROLLER_H_
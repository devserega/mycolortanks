#include "Controllers/AI/FleeCommand.h"
#include "Models/TankModel.h"
#include "Models/WorldModel.h"
#include "common.h"

FleeCommand::FleeCommand(TankModelRef tankModel, float duration)
	: BaseCommand(tankModel, duration) 
	, mInit(false)
{
}

FleeCommand::~FleeCommand() {
	if (mAITankModel && mAITankModel->getPlatformModel()) {
		mAITankModel->getPlatformModel()->move(PlatformModel::MoveDirection::NONE);
	}
}

bool FleeCommand::execute(float dt) {
	if (BaseCommand::execute(dt) && mAITankModel) {
		auto worldModel = mAITankModel->getWorldModel();
		if (!worldModel)
			return false;

		auto playerTank = worldModel->findPlayerTank();
		if (!playerTank)
			return false;

		if (!mInit) {
			mInit = true;

			auto aiTankPosPos = mAITankModel->getPosition();
			auto playerTankPos = playerTank->getPosition();
			mPlatformAngleRadians = atan2(playerTankPos.y - aiTankPosPos.y, playerTankPos.x - aiTankPosPos.x);
		}

		float angleDegree = RAD_TO_DEG(mPlatformAngleRadians);
		auto aiPlatformModel = mAITankModel->getPlatformModel();
		if (!aiPlatformModel)
			return false;

		aiPlatformModel->move(PlatformModel::MoveDirection::FORWARD);
		aiPlatformModel->rotate(-angleDegree);
		aiPlatformModel->setMoveSpeed(PLAYER_MOVE_SPEED * 1.2f);
	}

	return true;
}
#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _PURSUIT_COMMAND_H_
#define _PURSUIT_COMMAND_H_
//----------------------------------------------------------------------------------------------------
#include "BaseCommand.h"
//----------------------------------------------------------------------------------------------------
class PursuitCommand;
typedef std::shared_ptr<PursuitCommand> PursuitCommandRef;
//----------------------------------------------------------------------------------------------------
class PursuitCommand
	: public BaseCommand
{
	public:
		PursuitCommand(TankModelRef tankModel, float duration);
		virtual ~PursuitCommand();
		virtual bool execute(float dt) override;
		virtual void setMoveSpeed(float moveSpeed) { mPlatformSpeed = moveSpeed;}
		virtual void setStopDistance(float stopDistance) { mStopDistance = stopDistance; }

	protected:	
		float mStopDistance;
		float mPlatformAngleRadians;
		float mPlatformSpeed;
		bool  mInit;
};
//----------------------------------------------------------------------------------------------------
#endif // _PURSUIT_COMMAND_H_
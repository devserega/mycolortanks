#pragma once
#ifndef _GREEN_TANK_AI_H_
#define _GREEN_TANK_AI_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "AIController.h"
//----------------------------------------------------------------------------------------------------
class BaseModel;
class GreenTankAI;
typedef std::shared_ptr<GreenTankAI> GreenTankAIRef;
//----------------------------------------------------------------------------------------------------
class GreenTankAI
	: public AIController
{
	public:
		enum class ModeType : size_t {
			PURSUIT = 0,
			FLEE = 1,
			COUNT = 2,
			NONE
		};

		static GreenTankAIRef create(TankModelRef tankModel);
		bool init();

		GreenTankAI(TankModelRef tankModel);
		virtual ~GreenTankAI();
		virtual void logic() override;

		/* BaseModelListener */
		void onDestroyed(BaseModel* model) override;

	protected:
		ModeType	mMode;
		float		mDistanceToPlayer;
};
//----------------------------------------------------------------------------------------------------
#endif // _GREEN_TANK_AI_H_
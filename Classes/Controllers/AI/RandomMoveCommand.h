#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _RANDOM_MOVE_COMMAND_H_
#define _RANDOM_MOVE_COMMAND_H_
//----------------------------------------------------------------------------------------------------
#include "BaseCommand.h"
//----------------------------------------------------------------------------------------------------
class PursuitCommand;
typedef std::shared_ptr<PursuitCommand> PursuitCommandRef;
//----------------------------------------------------------------------------------------------------
class RandomMoveCommand
	: public BaseCommand
{
	public:
		RandomMoveCommand(TankModelRef tankModel, float duration);
		virtual ~RandomMoveCommand();
		virtual bool execute(float dt) override;
		virtual void setMoveSpeed(float moveSpeed) { mPlatformSpeed = moveSpeed; }
		virtual void setStopDistance(float stopDistance) { mStopDistance = stopDistance; }

	protected:
		float mStopDistance;
		float mPlatformSpeed;
		bool  mInit;
		float mPlatformAngleDegree;
};
//----------------------------------------------------------------------------------------------------
#endif // _RANDOM_MOVE_COMMAND_H_
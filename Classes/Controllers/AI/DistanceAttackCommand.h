#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _DISTANCE_ATTACK_COMMAND_H_
#define _DISTANCE_ATTACK_COMMAND_H_
//----------------------------------------------------------------------------------------------------
#include "BaseCommand.h"
//----------------------------------------------------------------------------------------------------
class DistanceAttackCommand;
typedef std::shared_ptr<DistanceAttackCommand> DistanceAttackCommandRef;
//----------------------------------------------------------------------------------------------------
class DistanceAttackCommand
	: public BaseCommand
{
	public:
		DistanceAttackCommand(TankModelRef tankModel, float duration);
		virtual ~DistanceAttackCommand();
		virtual bool execute(float dt) override;
		virtual void setAttackDistance(float attackDistance) { mAttackDistance = attackDistance; }

	protected:
		float mAttackDistance;
};
//----------------------------------------------------------------------------------------------------
#endif // _DISTANCE_ATTACK_COMMAND_H_
#pragma once
#ifndef _YELLOW_TANK_AI_H_
#define _YELLOW_TANK_AI_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "AIController.h"
//----------------------------------------------------------------------------------------------------
class BaseModel;
class YellowTankAI;
typedef std::shared_ptr<YellowTankAI> YellowTankAIRef;
//----------------------------------------------------------------------------------------------------
class YellowTankAI
	: public AIController
{
	public:
		static YellowTankAIRef create(TankModelRef tankModel);
		bool init();

		YellowTankAI(TankModelRef tankModel);
		virtual ~YellowTankAI();
		virtual void logic() override;

		/* BaseModelListener */
		void onDestroyed(BaseModel* model) override;
};
//----------------------------------------------------------------------------------------------------
#endif // _YELLOW_TANK_AI_H_
#pragma once
#ifndef _RED_TANK_AI_H_
#define _RED_TANK_AI_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "AIController.h"
//----------------------------------------------------------------------------------------------------
class BaseModel;
class RedTankAI;
typedef std::shared_ptr<RedTankAI> RedTankAIRef;
//----------------------------------------------------------------------------------------------------
class RedTankAI
	: public AIController
{
	public:
		enum class ModeType : size_t {
			PURSUIT = 0,
			FLEE = 1,
			COUNT = 2,
			NONE
		};

		static RedTankAIRef create(TankModelRef tankModel);
		bool init();

		RedTankAI(TankModelRef tankModel);
		virtual ~RedTankAI();
		virtual void logic() override;

		/* BaseModelListener */
		void onDestroyed(BaseModel* model) override;

	protected:
		ModeType	mMode;
		float		mLastDistance;
};
//----------------------------------------------------------------------------------------------------
#endif // _RED_TANK_AI_H_
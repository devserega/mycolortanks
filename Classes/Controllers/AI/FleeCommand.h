#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _FLEE_COMMAND_H_
#define _FLEE_COMMAND_H_
//----------------------------------------------------------------------------------------------------
#include "BaseCommand.h"
//----------------------------------------------------------------------------------------------------
class FleeCommand;
typedef std::shared_ptr<FleeCommand> FleeCommandRef;
//----------------------------------------------------------------------------------------------------
class FleeCommand
	: public BaseCommand
{
	public:
		FleeCommand(TankModelRef tankModel, float duration);
		virtual ~FleeCommand();
		virtual bool execute(float dt) override;
		virtual void setMoveSpeed(float moveSpeed) { mPlatformSpeed = moveSpeed; }

	protected:
		float	mPlatformAngleRadians;
		float	mPlatformSpeed;
		bool	mInit;
};
//----------------------------------------------------------------------------------------------------
#endif // _FLEE_COMMAND_H_
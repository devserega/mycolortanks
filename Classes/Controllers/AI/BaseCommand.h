#pragma once
//----------------------------------------------------------------------------------------------------
#ifndef _BASE_COMMAND_H_
#define _BASE_COMMAND_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
//----------------------------------------------------------------------------------------------------
class TankModel;
typedef std::shared_ptr<TankModel> TankModelRef;
class BaseCommand;
typedef std::shared_ptr<BaseCommand> BaseCommandRef;
//----------------------------------------------------------------------------------------------------
class BaseCommand
{
	public:
		BaseCommand(TankModelRef aiTankModel, float duration) 
			: mAITankModel(aiTankModel)
			, mDuration(duration)
			, mDelay(0.0f) {}

		virtual ~BaseCommand() {}

		virtual bool execute(float dt) { 
			if (mDelay <= FLT_MIN && mDuration > FLT_MIN) {
				mDuration -= dt;

				return true;
			}
			else if(mDelay > FLT_MIN){
				mDelay = -dt;
			}

			return false;
		}

		virtual bool isFinished() { return mDuration < FLT_MIN; }
		virtual void setDelay(float delay) { mDelay = delay; }
		virtual void setDuration(float duration) { mDuration = duration; }

	protected:
		TankModelRef	mAITankModel;
		float			mDuration;
		float			mDelay;
};
//----------------------------------------------------------------------------------------------------
#endif // _BASE_COMMAND_H_
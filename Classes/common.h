#pragma once
#ifndef _COMMON_H_
#define _COMMON_H_
//----------------------------------------------------------------------------------------------------
const float PLAYER_BULLET_SPEED = 300.0f;
const float PLAYER_MOVE_SPEED = 100.0f;
const float PLAYER_PLATFORM_ROTATION_SPEED = 200.0f;
const size_t PLAYER_CURR_ARMOR = 20;
const size_t PLAYER_MAX_ARMOR = 20;
const float STOP_DISTANCE = 200.0f;
//----------------------------------------------------------------------------------------------------
#endif // _COMMON_H_
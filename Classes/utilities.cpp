#include "utilities.h"

namespace nmUtils{
	void rotate(std::vector<cocos2d::Point>& vertexes, const cocos2d::Point& pivot, float angleDegree)
	{
		for (size_t i = 0; i < vertexes.size(); ++i) {
			rotate(vertexes[i], pivot, angleDegree);
		}
	}

	void rotate(cocos2d::Point& vertex, const cocos2d::Point& pivot, float angleDegree)
	{
		float angleRadians = DEG_TO_RAD(angleDegree);

		// Shifting the pivot point to the origin 
		// and the given points accordingly 
		float x_shifted = vertex.x - pivot.x;
		float y_shifted = vertex.y - pivot.y;

		// Calculating the rotated point co-ordinates 
		// and shifting it back 
		vertex.x = pivot.x + (x_shifted*cos(angleRadians) - y_shifted * sin(angleRadians));
		vertex.y = pivot.y + (x_shifted*sin(angleRadians) + y_shifted * cos(angleRadians));
	}
}
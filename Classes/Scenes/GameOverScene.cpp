#include "GameOverScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "utilities.h"
#include "Scenes/BattleScene.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
using namespace ui;
//----------------------------------------------------------------------------------------------------
GameOverScene::GameOverScene() 
	: mRestartBtn(nullptr)
	, mPlayerScore(0)
	, mPlayerScoreLabel(nullptr){
}

GameOverScene::~GameOverScene() {
}

GameOverScene* GameOverScene::create() {
	GameOverScene* pRet = new (std::nothrow) GameOverScene();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

GameOverScene* GameOverScene::createDefault(size_t playerScore) {
	static Data data;
	if (0 == data.getSize()) {
		CSLoader::getInstance()->registReaderObject("GameOverSceneReader", (ObjectFactory::Instance) GameOverSceneReader::getInstance);
		data = FileUtils::getInstance()->getDataFromFile("Scenes/GameOverScene.csb");
	}

	GameOverScene* rootNode = static_cast<GameOverScene*>(CSLoader::createNode(data, GameOverSceneReader::sm_nodeLoadCallback));
	const Size visibleSize = Director::getInstance()->getVisibleSize();
	rootNode->setContentSize(visibleSize);
	ui::Helper::doLayout(rootNode);

	rootNode->mPlayerScore = playerScore;

	if (rootNode->mRestartBtn) {
		rootNode->mRestartBtn->addClickEventListener([](Ref*) {
			auto scene = BattleScene::createDefault();
			auto director = Director::getInstance();
			director->replaceScene(scene);
		});
	}

	if (rootNode->mPlayerScoreLabel) {
		auto value = dynamic_cast<Text*>(rootNode->mPlayerScoreLabel->getChildByName("value"));
		auto str = StringUtils::format("%d", rootNode->mPlayerScore);
		value->setString(str);
	}

	return rootNode;
}

bool GameOverScene::init() {
	if (!Parent::init()) {
		return false;
	}

	return true;
}

//--------------------------------------------------------------------------------------------------------------------------
GameOverScene* GameOverSceneReader::sm_pTmpNode = nullptr;
void GameOverSceneReader::sm_nodeLoadCallback(Ref* pRef) {
	if (GameOverSceneReader::sm_pTmpNode) {
		cocos2d::Node* pNode = dynamic_cast<cocos2d::Node*>(pRef);
		if (!pNode)
			return;

		std::string strName = pNode->getName();
		auto pTmpNode = GameOverSceneReader::sm_pTmpNode;

		if (strName.compare("restart_btn") == 0) {
			GameOverSceneReader::sm_pTmpNode->mRestartBtn = dynamic_cast<Widget*>(pNode);
		}
		else if (strName.compare("player_score_label") == 0) {
			GameOverSceneReader::sm_pTmpNode->mPlayerScoreLabel = dynamic_cast<Text*>(pNode);
		}
	}
}

static GameOverSceneReader* _instanceGameOverScene = nullptr;
GameOverSceneReader* GameOverSceneReader::getInstance() {
	if (!_instanceGameOverScene)
		_instanceGameOverScene = new GameOverSceneReader();

	return _instanceGameOverScene;
}

void GameOverSceneReader::purge() {
	CC_SAFE_DELETE(_instanceGameOverScene);
}

cocos2d::Node* GameOverSceneReader::createNodeWithFlatBuffers(const flatbuffers::Table* nodeGameOverScene) {
	GameOverScene* node = GameOverScene::create();
	sm_pTmpNode = node;
	setPropsWithFlatBuffers(node, nodeGameOverScene);

	return node;
}
#include "BattleScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "Views/TankView.h"
#include "Views/BulletView.h"
#include "utilities.h"
#include "Builders/PlayerTankBuilder.h"
#include "Builders/RedEnemyTankBuilder.h"
#include "Builders/GreenEnemyTankBuilder.h"
#include "Builders/YellowEnemyTankBuilder.h"
#include "Views/ExplosionView.h"
#include "Views/TankExplosionView.h"
#include "Controllers/AI/RedTankAI.h"
#include "Controllers/AI/GreenTankAI.h"
#include "Controllers/AI/YellowTankAI.h"
#include "Models/Cannons/SingleCannonModel.h"
#include "Models/Cannons/DualCannonModel.h"
#include "Models/Cannons/CrossCannonModel.h"
#include "common.h"
#include "Scenes/GameOverScene.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
using namespace ui;
//----------------------------------------------------------------------------------------------------
BattleScene::BattleScene()
	: mPlayerTankView(nullptr)
	, mMouseListener(nullptr)
	, mKeyboardEventListener(nullptr)
	, mBattlefield(nullptr)
	, mKeyMappingUpdated(false)
	, mPlayerLives(3)
	, mScore(0)
	, mPlayerScoreLabel(nullptr)
	, mPlayerLivesLabel(nullptr)
	, mCurrWeaponLabel(nullptr)
	, mGroundEffectsLayer(nullptr)
	, mCurrPlayerWeaponIdx(0)
	, mEnemiesKilled(0)
	, mEnemiesTankInGame(0)
	, mGameState(GameState::EARLY)
{
	mWorldModel = std::make_unique<WorldModel>();

	mPlayerWeapons.push_back(CannonModel::Type::SINGLE);
	mPlayerWeapons.push_back(CannonModel::Type::DUAL);
	mPlayerWeapons.push_back(CannonModel::Type::CROSS);
}

BattleScene::~BattleScene() {
	CC_SAFE_RELEASE_NULL(mDrawNode);
}

BattleScene* BattleScene::create(){ 
	BattleScene *pRet = new (std::nothrow) BattleScene();
	if (pRet && pRet->init()) { 
		pRet->autorelease();
		return pRet; 
	} 
	else { 
		delete pRet; 
		pRet = nullptr; 
		return nullptr; 
	} 
}

BattleScene* BattleScene::createDefault(){
	static Data data;
	if (0 == data.getSize()){
		CSLoader::getInstance()->registReaderObject("BulletViewReader", (ObjectFactory::Instance) BulletViewReader::getInstance);
		CSLoader::getInstance()->registReaderObject("TankViewReader", (ObjectFactory::Instance) TankViewReader::getInstance);
		CSLoader::getInstance()->registReaderObject("BattleSceneReader", (ObjectFactory::Instance) BattleSceneReader::getInstance);
		data = FileUtils::getInstance()->getDataFromFile("Scenes/BattleScene.csb");
	}

	BattleScene* rootNode = static_cast<BattleScene*>(CSLoader::createNode(data, BattleSceneReader::sm_nodeLoadCallback));
	const Size visibleSize = Director::getInstance()->getVisibleSize();
	rootNode->setContentSize(visibleSize);
	ui::Helper::doLayout(rootNode);

	rootNode->mBattlefield = nmUtils::findChildNode<Layout>(rootNode, "battlefield_layer");
	if (rootNode->mBattlefield) {
		rootNode->mWorldModel->setWorldBorders(rootNode->mBattlefield->getBoundingBox());

		auto playerSpawnPoint = nmUtils::findChildNode<Node>(rootNode->mBattlefield, "playerSpawnPoint");
		if (playerSpawnPoint) {
			playerSpawnPoint->removeAllChildren();
			rootNode->createPlayerTank(playerSpawnPoint);
		}

		auto childrens = rootNode->mBattlefield->getChildren();
		for (auto child : childrens) {
			int32_t index = -1;
			int retval = sscanf(child->getName().c_str(), "enemySpawnPoint%d", &index);
			if (retval == 1 && index != -1) {
				rootNode->mEnemySpawnPoints.push_back(child);
				child->removeAllChildren();
			}
		}

		rootNode->mGameState = GameState::EARLY;
		for (int i = 1; i <= 5; ++i) {
			int idx = cocos2d::RandomHelper::random_int(1, (int)rootNode->mEnemySpawnPoints.size() - 1);
			rootNode->createEnemyTank(rootNode->mEnemySpawnPoints[idx], PlatformModel::Type::GREEN);
		}

		// for test
		rootNode->mDrawNode = DrawNode::create();
		rootNode->mDrawNode->retain();
		rootNode->mDrawNode->setLineWidth(4);
		rootNode->mBattlefield->addChild(rootNode->mDrawNode, 2);
	}

	rootNode->mGroundEffectsLayer = nmUtils::findChildNode<Layout>(rootNode, "ground_effects_layer");

	rootNode->mWorldModel->addWorldModelListener(rootNode);

	rootNode->refreshScore();
	rootNode->refreshPlayerLives();
	rootNode->refreshCurrWeaponIcon();

	rootNode->mKeyMapping[EventKeyboard::KeyCode::KEY_A] = false;
	rootNode->mKeyMapping[EventKeyboard::KeyCode::KEY_D] = false;
	rootNode->mKeyMapping[EventKeyboard::KeyCode::KEY_W] = false;
	rootNode->mKeyMapping[EventKeyboard::KeyCode::KEY_S] = false;

	return rootNode;
}

bool BattleScene::init() {
	if (!Parent::init()){
		return false;
	}

	return true;
}

void BattleScene::onEnter() {
	Scene::onEnter();
	//schedule(CC_CALLBACK_1(BattleArenaScene::update, this), 1.0f / 60.0f, "update");

	// Mouse listner
	mMouseListener = EventListenerMouse::create();
	mMouseListener->onMouseMove = CC_CALLBACK_1(BattleScene::onMouseMove, this);
	mMouseListener->onMouseUp = CC_CALLBACK_1(BattleScene::onMouseUp, this);
	mMouseListener->onMouseDown = CC_CALLBACK_1(BattleScene::onMouseDown, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mMouseListener, this);

	// Keyboard listner
	mKeyboardEventListener = EventListenerKeyboard::create();
	mKeyboardEventListener->onKeyPressed = CC_CALLBACK_2(BattleScene::onKeyboardPressed, this);
	mKeyboardEventListener->onKeyReleased = CC_CALLBACK_2(BattleScene::onKeyboardReleased, this);
	mKeyboardEventListener->setEnabled(true); // ����� ������� ������ �����, ����� �����
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mKeyboardEventListener, this);
}

void BattleScene::onExit() {
	Scene::onExit();
	//unschedule("update");

	Director::getInstance()->getEventDispatcher()->removeEventListener(mMouseListener);
	Director::getInstance()->getEventDispatcher()->removeEventListener(mKeyboardEventListener);
}

void BattleScene::drawBoundingBox() {
}

void BattleScene::createEnemyTank(Node* spawnPoint, PlatformModel::Type tankType) {
	// create enemy tank model
	TankBuilderRef tankBuilder = nullptr;
	switch (tankType) {
		case PlatformModel::Type::RED:
			tankBuilder = std::make_unique<RedEnemyTankBuilder>();
			break;
		case PlatformModel::Type::GREEN:
			tankBuilder = std::make_unique<GreenEnemyTankBuilder>();
			break;
		case PlatformModel::Type::YELLOW:
			tankBuilder = std::make_unique<YellowEnemyTankBuilder>();
			break;
	}

	if (!tankBuilder)
		return;
	
	tankBuilder->createNewTankModel();
	tankBuilder->addPlatform();
	tankBuilder->addCannon();

	auto enemyTankModel = tankBuilder->GetTankModel();
	if (enemyTankModel) {
		enemyTankModel->setWorldModel(mWorldModel);
		enemyTankModel->setPosition(spawnPoint->getPosition());
		mWorldModel->addModel(enemyTankModel);

		auto platformModel = enemyTankModel->getPlatformModel();
		if (platformModel) {
			platformModel->addPlatformModelListener(this);
			//platformModel->setRotation(90);
		}

		auto cannonModel = enemyTankModel->getCannonModel();
		if (cannonModel) {
			cannonModel->addCannonModelListener(this);
		}

		// create enemy tank view
		auto enemyTankView = TankView::createDefault(enemyTankModel);
		mBattlefield->addChild(enemyTankView);

		// create enemy tank ai
		AIControllerRef ai = nullptr;
		switch (tankType) {
			case PlatformModel::Type::RED:
				ai = std::make_shared<RedTankAI>(enemyTankModel);
				break;
			case PlatformModel::Type::GREEN:
				ai = std::make_shared<GreenTankAI>(enemyTankModel);
				break;
			case PlatformModel::Type::YELLOW:
				ai = std::make_shared<YellowTankAI>(enemyTankModel);
				break;
		}

		if (ai) {
			mWorldModel->addController(ai);
		}	

		mEnemiesTankInGame++;
	}
}

void BattleScene::createPlayerTank(Node* spawnPoint) {
	// create player tank model
	auto tankBuilder = std::make_unique<PlayerTankBuilder>();
	tankBuilder->createNewTankModel();
	tankBuilder->addPlatform();
	tankBuilder->addCannon();

	auto playerTankModel = tankBuilder->GetTankModel();
	playerTankModel->setWorldModel(mWorldModel);
	playerTankModel->setPosition(spawnPoint->getPosition());
	playerTankModel->addTankModelListener(this);
	mWorldModel->addModel(playerTankModel);

	auto platformModel = playerTankModel->getPlatformModel();
	if (platformModel) {
		platformModel->addPlatformModelListener(this);
	}

	auto cannonModel = playerTankModel->getCannonModel();
	if (cannonModel) {
		cannonModel->addCannonModelListener(this);
	}

	// create player tank view
	mPlayerTankView = TankView::createDefault(playerTankModel);
	mBattlefield->addChild(mPlayerTankView);
}

void BattleScene::update(float dt) {
	auto playerTankModel = mWorldModel->findPlayerTank();
	if (playerTankModel && mKeyMappingUpdated) {
		auto platfromModel = playerTankModel->getPlatformModel();
		mKeyMappingUpdated = false;

		if (platfromModel && mKeyMapping[EventKeyboard::KeyCode::KEY_A]) {
			platfromModel->move(PlatformModel::MoveDirection::FORWARD);
			platfromModel->rotate(180.0f);
		}
		else if (platfromModel && mKeyMapping[EventKeyboard::KeyCode::KEY_D]) {
			platfromModel->move(PlatformModel::MoveDirection::FORWARD);
			platfromModel->rotate(0.0f);
		}
		else if (platfromModel && mKeyMapping[EventKeyboard::KeyCode::KEY_W]) {
			platfromModel->move(PlatformModel::MoveDirection::FORWARD);
			platfromModel->rotate(90.0f);
		}
		else if (platfromModel && mKeyMapping[EventKeyboard::KeyCode::KEY_S]) {
			platfromModel->move(PlatformModel::MoveDirection::FORWARD);
			platfromModel->rotate(270.0f);
		}
		else if (platfromModel && mKeyMapping[EventKeyboard::KeyCode::KEY_Q]) {
			switchPreviousWeapon();
		}
		else if (platfromModel && mKeyMapping[EventKeyboard::KeyCode::KEY_E]) {
			switchNextWeapon();
		}
		else if(platfromModel){
			platfromModel->move(PlatformModel::MoveDirection::NONE);
		}
	}

	if (mWorldModel) {
		mWorldModel->update(dt);
	}

	mDrawNode->clear();

	// test draw direction vector
	if (playerTankModel) {
		auto platformModel = playerTankModel->getPlatformModel();
		if (platformModel) {
			/*
			// draw debug model info
			auto models = mWorldModel->getModelsList();
			for (size_t i = 0; i < models.size(); ++i) {
				auto model = models[i];
				if (model) {
					auto vertixes = model->getBoundingBox(model->getPosition(), model->getRotation());
					if (vertixes.size() == 4) {
						mDrawNode->drawLine(vertixes[0], vertixes[1], Color4F(0.34f, 0.34f, 0.34f, 0.7f));
						mDrawNode->drawLine(vertixes[1], vertixes[2], Color4F(0.34f, 0.34f, 0.34f, 0.7f));
						mDrawNode->drawLine(vertixes[2], vertixes[3], Color4F(0.34f, 0.34f, 0.34f, 0.7f));
						mDrawNode->drawLine(vertixes[3], vertixes[0], Color4F(0.34f, 0.34f, 0.34f, 0.7f));
					}
				}		
			}
			*/
		}
	}
}

void BattleScene::onMouseMove(cocos2d::EventMouse* pEvent) {
	auto playerTankModel = mWorldModel->findPlayerTank();
	if (playerTankModel) {
		auto playerTankPos = playerTankModel->getPosition();
		auto locationInNode = mBattlefield->convertToNodeSpaceAR(pEvent->getLocationInView());
		float angleRadians = atan2(locationInNode.y - playerTankPos.y, locationInNode.x - playerTankPos.x);
		//CCLOG("onMouseMove (%f;%f)", pEvent->getLocationInView().x, pEvent->getLocationInView().y);

		auto cannonModel = playerTankModel->getCannonModel();
		if (cannonModel) {
			float angleDegree = RAD_TO_DEG(angleRadians);
			cannonModel->setRotation(angleDegree);
		}
	}
}

void BattleScene::onMouseDown(cocos2d::EventMouse* pEvent) {
	auto playerTankModel = mWorldModel->findPlayerTank();
	if (playerTankModel) {
		auto cannonModel = playerTankModel->getCannonModel();
		if (!cannonModel)
			return;

		cannonModel->fire(CannonModel::FireState::ACTIVE);
	}
}

void BattleScene::onMouseUp(cocos2d::EventMouse* pEvent) {
	auto playerTankModel = mWorldModel->findPlayerTank();
	if (playerTankModel) {
		auto cannonModel = playerTankModel->getCannonModel();
		if (!cannonModel)
			return;

		cannonModel->fire(CannonModel::FireState::NONE);
	}
}

void BattleScene::onKeyboardPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* pEvent)
{
	if (pEvent == nullptr)
		return;

	mKeyMapping[keyCode] = true;
	mKeyMappingUpdated = true;

	pEvent->stopPropagation();
}

void BattleScene::onKeyboardReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* pEvent) {
	if (pEvent == nullptr)
		return;

	mKeyMapping[keyCode] = false;
	mKeyMappingUpdated = true;
}

void BattleScene::onPlatformRotated(float angle) {
}

void BattleScene::onPlatformMoved(const Vec2& delta) {
	/*
	if (mBackground) {
		Vec2 newPos = mBackground->getPosition()-delta;
		CheckCameraPos(newPos);
		mBackground->setPosition(newPos);
	}
	*/
}

void BattleScene::onCannonRotated(float angle) {
	// ...
}

void BattleScene::onCannonFired(std::vector<BulletModelRef> bullets) {
	if (bullets.size()>0) {
		for (size_t i = 0; i < bullets.size();++i) {
			mWorldModel->addModel(bullets[i]);
		}
	}
}

void BattleScene::onDestroyed(BaseModel* model) {
	auto tankModel = dynamic_cast<TankModel*>(model);
	auto bulletModel = dynamic_cast<BulletModel*>(model);
	if (tankModel && !tankModel->isPlayerTank()) {
		mEnemiesKilled++;
		mEnemiesTankInGame--;
		mScore += tankModel->getBounty();
		refreshScore();
		
		auto explosion = TankExplosionView::create();
		explosion->setPosition(tankModel->getPosition());
		mGroundEffectsLayer->addChild(explosion, 1);
	}
	else if (tankModel && tankModel->isPlayerTank()) {
		if (mPlayerLives> 0) {
			mPlayerLives--;
			refreshPlayerLives();

			auto playerSpawnPoint = nmUtils::findChildNode<Node>(mBattlefield, "playerSpawnPoint");
			if (playerSpawnPoint) {
				playerSpawnPoint->removeAllChildren();
				createPlayerTank(playerSpawnPoint);
			}
		}
		else {
			auto scene = GameOverScene::createDefault(mScore);
			auto director = Director::getInstance();
			director->replaceScene(scene);
		}
	}
	else if (bulletModel) {
		auto angleDegree = cocos2d::RandomHelper::random_int(1, 360);
		auto scale = cocos2d::RandomHelper::random_real(0.7f, 1.0f);

		auto explosion = ExplosionView::create();
		explosion->setPosition(bulletModel->getPosition());
		Color3B color = bulletModel->getColor();
		explosion->setColor(color);
		explosion->setRotation(angleDegree);
		explosion->setScale(scale);
		mGroundEffectsLayer->addChild(explosion, 1);
	}

	generateNewEnemies();
}

void BattleScene::refreshScore() {
	if (mPlayerScoreLabel) {
		auto value = nmUtils::findChildNode<Text>(mPlayerScoreLabel, "value");
		if (value) {
			auto str = StringUtils::format("%d", mScore);
			value->setString(str);
		}
	}
}

void BattleScene::refreshPlayerLives() {
	if (mPlayerLivesLabel) {
		auto value = nmUtils::findChildNode<Text>(mPlayerLivesLabel, "value");
		if (value) {
			auto str = StringUtils::format("%d", mPlayerLives);
			value->setString(str);
		}
	}
}

void BattleScene::refreshCurrWeaponIcon() {
	auto playerTankModel = mWorldModel->findPlayerTank();
	if (mCurrWeaponLabel && playerTankModel) {
		auto icon = nmUtils::findChildNode<Sprite>(mCurrWeaponLabel, "icon");
		if (icon) {
			auto cannonType = playerTankModel->getCannonModel()->getType();
			switch (cannonType) {
			case CannonModel::Type::SINGLE:
				icon->setTexture("studio/Textures/single_cannon.png");
				break;
			case CannonModel::Type::DUAL:
				icon->setTexture("studio/Textures/double_cannon.png");
				break;
			case CannonModel::Type::CROSS:
				icon->setTexture("studio/Textures/cross_cannon.png");
				break;
			}
		}
	}
}

void BattleScene::switchNextWeapon() {
	auto playerTankModel = mWorldModel->findPlayerTank();
	if (playerTankModel) {
		size_t i = (++mCurrPlayerWeaponIdx) % mPlayerWeapons.size();
		auto cannonType = static_cast<CannonModel::Type>(i);
		changePlayerWeapon(cannonType, playerTankModel);
	}
}

void BattleScene::switchPreviousWeapon() {
	auto playerTankModel = mWorldModel->findPlayerTank();
	if (playerTankModel) {
		size_t i = (--mCurrPlayerWeaponIdx) % mPlayerWeapons.size();
		auto cannonType = static_cast<CannonModel::Type>(i);
		changePlayerWeapon(cannonType, playerTankModel);
	}
}

void BattleScene::changePlayerWeapon(CannonModel::Type cannonType, TankModelRef playerTankModel) {
	if (cannonType == CannonModel::Type::NONE)
		return;

	auto angle = playerTankModel->getCannonModel()->getRotation();

	if (cannonType == CannonModel::Type::DUAL) {
		DualCannonModelRef cannonModel = std::make_shared<DualCannonModel>();
		cannonModel->setWorldModel(mWorldModel);
		cannonModel->setStartBulletSpeed(500.0f);
		cannonModel->setReloadTime(0.15f);
		cannonModel->setOwner(playerTankModel);
		cannonModel->setTeamIndex(1);
		cannonModel->setRotation(angle);
		cannonModel->addCannonModelListener(this);
		playerTankModel->setCannon(cannonModel);
	}
	else if (cannonType == CannonModel::Type::SINGLE) {
		SingleCannonModelRef cannonModel = std::make_shared<SingleCannonModel>();
		cannonModel->setWorldModel(mWorldModel);
		cannonModel->setStartBulletSpeed(PLAYER_BULLET_SPEED);
		cannonModel->setReloadTime(1.0f);
		cannonModel->setOwner(playerTankModel);
		cannonModel->setTeamIndex(1);
		cannonModel->setRotation(angle);
		cannonModel->addCannonModelListener(this);
		playerTankModel->setCannon(cannonModel);
	}
	else if (cannonType == CannonModel::Type::CROSS) {
		CrossCannonModelRef cannonModel = std::make_shared<CrossCannonModel>();
		cannonModel->setWorldModel(mWorldModel);
		cannonModel->setStartBulletSpeed(500.0f);
		cannonModel->setReloadTime(0.2f);
		cannonModel->setTeamIndex(1);
		cannonModel->setOwner(playerTankModel);
		cannonModel->setRotation(angle);
		cannonModel->addCannonModelListener(this);
		playerTankModel->setCannon(cannonModel);
	}
}

void BattleScene::onSetPlatform(TankModel& tankModel) {

}

void BattleScene::onSetCannon(TankModel& tankModel) {
	refreshCurrWeaponIcon();
}

void BattleScene::onChangedArmor(TankModel& tankModel) {

}

void BattleScene::generateNewEnemies() {
	if (mGameState == GameState::EARLY && mEnemiesKilled >= 3) {
		mGameState = GameState::MIDDLE;

		for (size_t i = 1; i <= 2; ++i) {
			int idx = cocos2d::RandomHelper::random_int(1, (int)mEnemySpawnPoints.size() - 1);
			createEnemyTank(mEnemySpawnPoints[idx], PlatformModel::Type::YELLOW);
		}
	}
	else if (mGameState == GameState::MIDDLE && mEnemiesTankInGame <= 4) {
		size_t count = 6 - mEnemiesTankInGame;
		for (size_t i = 1; i <= count; ++i) {
			int idx = cocos2d::RandomHelper::random_int(1, (int)mEnemySpawnPoints.size() - 1);
			int tankType = cocos2d::RandomHelper::random_int(1, 3);
			switch (tankType) {
			case 1:
				createEnemyTank(mEnemySpawnPoints[idx], PlatformModel::Type::GREEN);
				break;
			case 2:
				createEnemyTank(mEnemySpawnPoints[idx], PlatformModel::Type::YELLOW);
				break;
			case 3:
				createEnemyTank(mEnemySpawnPoints[idx], PlatformModel::Type::RED);
				break;
			}
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------
BattleScene* BattleSceneReader::sm_pTmpNode = nullptr;
void BattleSceneReader::sm_nodeLoadCallback(Ref* pRef) {
	if (BattleSceneReader::sm_pTmpNode) {
		cocos2d::Node* pNode = dynamic_cast<cocos2d::Node*>(pRef);
		if (!pNode)
			return;

		std::string strName = pNode->getName();
		auto pTmpNode = BattleSceneReader::sm_pTmpNode;

		if (strName.compare("playerLivesLabel") == 0) {
			BattleSceneReader::sm_pTmpNode->mPlayerLivesLabel = dynamic_cast<Text*>(pNode);
		}
		else if (strName.compare("scoreLabel") == 0) {
			BattleSceneReader::sm_pTmpNode->mPlayerScoreLabel = dynamic_cast<Text*>(pNode);
		}
		else if (strName.compare("weaponLabel") == 0) {
			BattleSceneReader::sm_pTmpNode->mCurrWeaponLabel= dynamic_cast<Text*>(pNode);
		}
	}
}

static BattleSceneReader* _instanceBattleScene = nullptr;
BattleSceneReader* BattleSceneReader::getInstance() {
	if (!_instanceBattleScene)
		_instanceBattleScene = new BattleSceneReader();

	return _instanceBattleScene;
}

void BattleSceneReader::purge() {
	CC_SAFE_DELETE(_instanceBattleScene);
}

cocos2d::Node* BattleSceneReader::createNodeWithFlatBuffers(const flatbuffers::Table* nodeBattleScene) {
	BattleScene* node = BattleScene::create();
	sm_pTmpNode = node;
	setPropsWithFlatBuffers(node, nodeBattleScene);

	return node;
}
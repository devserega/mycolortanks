#pragma once
#ifndef _BATTLE_SCENE_H_
#define _BATTLE_SCENE_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "editor-support/cocostudio/WidgetCallBackHandlerProtocol.h"
#include "editor-support/cocostudio/WidgetReader/NodeReader/NodeReader.h"

#include "Models/WorldModel.h"
//----------------------------------------------------------------------------------------------------
class TankView;
class BattleScene
	: public cocos2d::Scene
	, public IPlatformModelListener
	, public ICannonModelListener
	, public IWorldModelListener
	, public ITankModelListener
{
		friend class BattleSceneReader;
		typedef cocos2d::Scene Parent;

	public:
		static BattleScene* create();
		static BattleScene* createDefault();
		virtual bool init();

	protected:
		enum class GameState : size_t {
			EARLY,
			MIDDLE
		};

		WorldModelRef mWorldModel;
		TankView* mPlayerTankView;
		cocos2d::EventListenerMouse* mMouseListener;
		cocos2d::EventListenerKeyboard* mKeyboardEventListener;
		cocos2d::DrawNode* mDrawNode;
		cocos2d::ui::Layout* mBattlefield;
		std::unordered_map<cocos2d::EventKeyboard::KeyCode, bool> mKeyMapping;
		bool mKeyMappingUpdated;
		size_t mPlayerLives;
		size_t mScore;
		cocos2d::ui::Text* mPlayerScoreLabel;
		cocos2d::ui::Text* mPlayerLivesLabel;
		cocos2d::ui::Text* mCurrWeaponLabel;
		cocos2d::ui::Layout* mGroundEffectsLayer;
		std::vector<cocos2d::Node*> mEnemySpawnPoints;
		std::vector<CannonModel::Type> mPlayerWeapons;
		size_t mCurrPlayerWeaponIdx;
		size_t mEnemiesKilled;
		size_t mEnemiesTankInGame;
		GameState mGameState;

		void onEnter() override;
		void onExit() override;
		virtual void update(float dt);
		void onMouseMove(cocos2d::EventMouse* pEvent);
		void onMouseDown(cocos2d::EventMouse* pEvent);
		void onMouseUp(cocos2d::EventMouse* pEvent);
		void onKeyboardPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* pEvent);
		void onKeyboardReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* pEvent);
		void onPlatformRotated(float angle) override;
		void onPlatformMoved(const cocos2d::Vec2& deltaMove) override;
		void drawBoundingBox();
		void createEnemyTank(cocos2d::Node* spawnPoint, PlatformModel::Type tankType);
		void createPlayerTank(cocos2d::Node* spawnPoint);
		void refreshScore();
		void refreshPlayerLives();
		void refreshCurrWeaponIcon();
		void switchNextWeapon();
		void switchPreviousWeapon();
		void changePlayerWeapon(CannonModel::Type cannonType, TankModelRef playerTankModel);
		void generateNewEnemies();

		/* BattleScene */
		void onCannonRotated(float angle) override;
		void onCannonFired(std::vector<BulletModelRef> bullets) override;

		/* IWorldModelListener */
		void onDestroyed(BaseModel* model) override;

		/* ITankModelListener */
		void onSetPlatform(TankModel& tankModel) override;
		void onSetCannon(TankModel& tankModel) override;
		void onChangedArmor(TankModel& tankModel) override;

	public:
		virtual ~BattleScene();

	private:
		BattleScene();
};
//--------------------------------------------------------------------------------------------------------------------------
class BattleSceneReader
	: public cocostudio::NodeReader
{
	public:
		static BattleScene* sm_pTmpNode;
		static void sm_nodeLoadCallback(cocos2d::Ref * pRef);

		BattleSceneReader() {};
		~BattleSceneReader() {};
		static BattleSceneReader* getInstance();
		static void purge();
		cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeBattleArenaScene);
};
//----------------------------------------------------------------------------------------------------
#endif // _BATTLE_SCENE_H_
#pragma once
#ifndef _GAMEOVER_SCENE_H_
#define _GAMEOVER_SCENE_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "editor-support/cocostudio/WidgetCallBackHandlerProtocol.h"
#include "editor-support/cocostudio/WidgetReader/NodeReader/NodeReader.h"
//----------------------------------------------------------------------------------------------------
class TankView;
class GameOverScene
	: public cocos2d::Scene
{
		friend class GameOverSceneReader;
		typedef cocos2d::Scene Parent;

	public:
		static GameOverScene* create();
		static GameOverScene* createDefault(size_t playerScore);
		virtual bool init();

	protected:
		cocos2d::ui::Widget* mRestartBtn;
		size_t mPlayerScore;
		cocos2d::ui::Text* mPlayerScoreLabel;

	public:
		virtual ~GameOverScene();
		void setScore(size_t score) { mPlayerScore = score; }

	private:
		GameOverScene();
};
//--------------------------------------------------------------------------------------------------------------------------
class GameOverSceneReader
	: public cocostudio::NodeReader
{
	public:
		static GameOverScene* sm_pTmpNode;
		static void sm_nodeLoadCallback(cocos2d::Ref* pRef);

		GameOverSceneReader() {};
		~GameOverSceneReader() {};
		static GameOverSceneReader* getInstance();
		static void purge();
		cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeGameOverScene);
};
//----------------------------------------------------------------------------------------------------
#endif // _GAMEOVER_SCENE_H_
#include "MainMenuScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "utilities.h"
#include "Scenes/BattleScene.h"
//----------------------------------------------------------------------------------------------------
using namespace cocos2d;
using namespace ui;
//----------------------------------------------------------------------------------------------------
MainMenuScene::MainMenuScene() {
}

MainMenuScene::~MainMenuScene() {
}

MainMenuScene* MainMenuScene::create() {
	MainMenuScene* pRet = new (std::nothrow) MainMenuScene();
	if (pRet && pRet->init()) {
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

MainMenuScene* MainMenuScene::createDefault() {
	static Data data;
	if (0 == data.getSize()) {
		CSLoader::getInstance()->registReaderObject("MainMenuSceneReader", (ObjectFactory::Instance) MainMenuSceneReader::getInstance);
		data = FileUtils::getInstance()->getDataFromFile("Scenes/MainMenuScene.csb");
	}

	MainMenuScene* rootNode = static_cast<MainMenuScene*>(CSLoader::createNode(data, MainMenuSceneReader::sm_nodeLoadCallback));
	const Size visibleSize = Director::getInstance()->getVisibleSize();
	rootNode->setContentSize(visibleSize);
	ui::Helper::doLayout(rootNode);

	Widget* btnRate = nmUtils::findChildNode<Widget>(rootNode, "play_btn");
	if (btnRate){
		btnRate->addClickEventListener([&rootNode](Ref*){
			auto scene = BattleScene::createDefault();
			auto director = Director::getInstance();
			director->replaceScene(scene);
		});
		//addSpringEffectOnTouch(btnRate);
	};

	return rootNode;
}

bool MainMenuScene::init() {
	if (!Parent::init()) {
		return false;
	}

	return true;
}

//--------------------------------------------------------------------------------------------------------------------------
MainMenuScene* MainMenuSceneReader::sm_pTmpNode = nullptr;
void MainMenuSceneReader::sm_nodeLoadCallback(Ref* pRef) {
	if (MainMenuSceneReader::sm_pTmpNode) {
		cocos2d::Node* pNode = dynamic_cast<cocos2d::Node*>(pRef);
		if (!pNode)
			return;

		std::string strName = pNode->getName();
		auto pTmpNode = MainMenuSceneReader::sm_pTmpNode;
		/*
		if (strName.compare("playerLivesLabel") == 0) {
			MainMenuSceneReader::sm_pTmpNode->mPlayerLivesLabel = dynamic_cast<Text*>(pNode);
		}
		else if (strName.compare("scoreLabel") == 0) {
			MainMenuSceneReader::sm_pTmpNode->mPlayerScoreLabel = dynamic_cast<Text*>(pNode);
		}
		else if (strName.compare("weaponLabel") == 0) {
			MainMenuSceneReader::sm_pTmpNode->mCurrWeaponLabel = dynamic_cast<Text*>(pNode);
		}
		*/
	}
}

static MainMenuSceneReader* _instanceMainMenuScene = nullptr;
MainMenuSceneReader* MainMenuSceneReader::getInstance() {
	if (!_instanceMainMenuScene)
		_instanceMainMenuScene = new MainMenuSceneReader();

	return _instanceMainMenuScene;
}

void MainMenuSceneReader::purge() {
	CC_SAFE_DELETE(_instanceMainMenuScene);
}

cocos2d::Node* MainMenuSceneReader::createNodeWithFlatBuffers(const flatbuffers::Table* nodeMainMenuScene) {
	MainMenuScene* node = MainMenuScene::create();
	sm_pTmpNode = node;
	setPropsWithFlatBuffers(node, nodeMainMenuScene);

	return node;
}
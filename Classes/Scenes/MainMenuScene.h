#pragma once
#ifndef _MAINMENU_SCENE_H_
#define _MAINMENU_SCENE_H_
//----------------------------------------------------------------------------------------------------
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "editor-support/cocostudio/WidgetCallBackHandlerProtocol.h"
#include "editor-support/cocostudio/WidgetReader/NodeReader/NodeReader.h"
//----------------------------------------------------------------------------------------------------
class TankView;
class MainMenuScene
	: public cocos2d::Scene
{
		friend class MainMenuSceneReader;
		typedef cocos2d::Scene Parent;

	public:
		static MainMenuScene* create();
		static MainMenuScene* createDefault();
		virtual bool init();

	public:
		virtual ~MainMenuScene();

	private:
		MainMenuScene();
};
//--------------------------------------------------------------------------------------------------------------------------
class MainMenuSceneReader
	: public cocostudio::NodeReader
{
	public:
		static MainMenuScene* sm_pTmpNode;
		static void sm_nodeLoadCallback(cocos2d::Ref* pRef);

		MainMenuSceneReader() {};
		~MainMenuSceneReader() {};
		static MainMenuSceneReader* getInstance();
		static void purge();
		cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeMainMenuScene);
};
//----------------------------------------------------------------------------------------------------
#endif // _MAINMENU_SCENE_H_
<GameFile>
  <PropertyGroup Name="Explosion" Type="Node" ID="181eb0d2-af53-4c3a-98c8-f9375ef5c5df" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="115" Speed="1.0000" ActivedAnimationName="default">
        <Timeline ActionTag="182309346" Property="Position">
          <PointFrame FrameIndex="115" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="182309346" Property="Scale">
          <ScaleFrame FrameIndex="115" X="0.2000" Y="0.2000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="182309346" Property="RotationSkew">
          <ScaleFrame FrameIndex="115" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="182309346" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="15" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="70" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="115" Value="0">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="default" StartIndex="0" EndIndex="115">
          <RenderColor A="150" R="255" G="255" B="0" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Node" Tag="3116" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="explosion" ActionTag="182309346" Alpha="187" Tag="3117" IconVisible="False" LeftMargin="-181.0000" RightMargin="-181.0000" TopMargin="-177.0000" BottomMargin="-177.0000" LeftEage="119" RightEage="119" TopEage="116" BottomEage="116" Scale9OriginX="119" Scale9OriginY="116" Scale9Width="124" Scale9Height="122" ctype="ImageViewObjectData">
            <Size X="362.0000" Y="354.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="0.2000" ScaleY="0.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Textures/explosion.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>
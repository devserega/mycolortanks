<GameFile>
  <PropertyGroup Name="UI" Type="Layer" ID="aba52f8d-d251-4300-9d1d-dac3fd245eaf" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="224" ctype="GameLayerObjectData">
        <Size X="1024.0000" Y="768.0000" />
        <Children>
          <AbstractNodeData Name="playerLivesLabel" ActionTag="-1527456899" Tag="579" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="32.5344" RightMargin="891.4656" TopMargin="27.4000" BottomMargin="718.6000" FontSize="20" LabelText="Tanks:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="22.0000" />
            <Children>
              <AbstractNodeData Name="value" ActionTag="-1630650582" Tag="580" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="100.0000" RightMargin="-55.0000" FontSize="20" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="100.0000" Y="11.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" Y="0.5000" />
                <PreSize X="0.5500" Y="1.0000" />
                <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="82.5344" Y="729.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0806" Y="0.9500" />
            <PreSize X="0.0977" Y="0.0286" />
            <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="scoreLabel" ActionTag="988771102" Tag="581" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="462.5000" RightMargin="462.5000" TopMargin="27.4000" BottomMargin="718.6000" FontSize="20" LabelText="Score:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="99.0000" Y="22.0000" />
            <Children>
              <AbstractNodeData Name="value" ActionTag="1576574085" Tag="582" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="100.0000" RightMargin="-56.0000" FontSize="20" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="100.0000" Y="11.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0101" Y="0.5000" />
                <PreSize X="0.5556" Y="1.0000" />
                <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="512.0000" Y="729.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9500" />
            <PreSize X="0.0967" Y="0.0286" />
            <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="weaponLabel" ActionTag="-417595072" Tag="583" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="809.2672" RightMargin="86.7328" TopMargin="27.4000" BottomMargin="718.6000" FontSize="20" LabelText="Weapon:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="128.0000" Y="22.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="-1473024944" Tag="713" IconVisible="False" LeftMargin="138.0000" RightMargin="-56.0000" TopMargin="-12.0000" BottomMargin="-12.0000" ctype="SpriteObjectData">
                <Size X="48.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="161.0000" Y="11.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.2578" Y="0.5000" />
                <PreSize X="0.3594" Y="2.0909" />
                <FileData Type="Normal" Path="Textures/single_cannon.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="873.2672" Y="729.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8528" Y="0.9500" />
            <PreSize X="0.1250" Y="0.0286" />
            <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>
<GameFile>
  <PropertyGroup Name="Bullet" Type="Node" ID="41c293a4-adcd-4a23-9028-7ae215b04936" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" CustomClassName="BulletView" Tag="240" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="shadow" ActionTag="-1299848509" Alpha="155" Tag="3092" IconVisible="False" LeftMargin="-13.6357" RightMargin="-12.3643" TopMargin="-2.6630" BottomMargin="-23.3370" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="11" Scale9OriginY="11" Scale9Width="4" Scale9Height="4" ctype="ImageViewObjectData">
            <Size X="26.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5244" ScaleY="0.4093" />
            <Position X="-0.0013" Y="-12.6952" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Textures/shadow.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cannonball" ActionTag="-1801600126" Tag="3091" IconVisible="False" LeftMargin="-13.0000" RightMargin="-13.0000" TopMargin="-13.0000" BottomMargin="-13.0000" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="10" Scale9Height="10" ctype="ImageViewObjectData">
            <Size X="26.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Textures/cannonball.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="blick" ActionTag="1463529399" Tag="211" IconVisible="False" LeftMargin="-5.9459" RightMargin="0.9459" TopMargin="-6.2217" BottomMargin="1.2217" LeftEage="1" RightEage="1" TopEage="1" BottomEage="1" Scale9OriginX="1" Scale9OriginY="1" Scale9Width="3" Scale9Height="3" ctype="ImageViewObjectData">
            <Size X="5.0000" Y="5.0000" />
            <AnchorPoint ScaleX="0.3069" ScaleY="0.5137" />
            <Position X="-4.4113" Y="3.7904" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Textures/blick.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>
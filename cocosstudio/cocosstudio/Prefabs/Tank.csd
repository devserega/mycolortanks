<GameFile>
  <PropertyGroup Name="Tank" Type="Node" ID="ecd557e8-1bc8-4ce2-ad8c-ae3f4df0e0f1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" CustomClassName="TankView" Tag="5" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="armor_bar" ActionTag="1181339545" Tag="138" IconVisible="False" LeftMargin="-33.5612" RightMargin="-36.4388" TopMargin="-62.6311" BottomMargin="50.6311" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="70.0000" Y="12.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.4388" Y="56.6311" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="165" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <ImageFileData Type="Normal" Path="Textures/healthbar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="platform_slot" ActionTag="-202009676" Tag="230" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="platform1" ActionTag="1488050209" VisibleForFrame="False" Tag="3113" IconVisible="False" LeftMargin="-26.5000" RightMargin="-26.5000" TopMargin="-35.0000" BottomMargin="-35.0000" LeftEage="17" RightEage="17" TopEage="23" BottomEage="23" Scale9OriginX="17" Scale9OriginY="23" Scale9Width="19" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/white_platform.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="platform2" ActionTag="1403920496" VisibleForFrame="False" Tag="3410" IconVisible="False" LeftMargin="-26.5000" RightMargin="-26.5000" TopMargin="-35.0000" BottomMargin="-35.0000" LeftEage="87" RightEage="87" TopEage="115" BottomEage="115" Scale9OriginX="-34" Scale9OriginY="-45" Scale9Width="121" Scale9Height="160" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/blue_platform.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="platform3" ActionTag="-1052741300" VisibleForFrame="False" Tag="3110" IconVisible="False" LeftMargin="-26.5000" RightMargin="-26.5000" TopMargin="-35.0000" BottomMargin="-35.0000" LeftEage="87" RightEage="87" TopEage="115" BottomEage="115" Scale9OriginX="-34" Scale9OriginY="-45" Scale9Width="121" Scale9Height="160" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/olive_platform.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="platform4" ActionTag="1634962757" VisibleForFrame="False" Tag="3111" IconVisible="False" LeftMargin="-26.5000" RightMargin="-26.5000" TopMargin="-35.0000" BottomMargin="-35.0000" LeftEage="87" RightEage="87" TopEage="115" BottomEage="115" Scale9OriginX="-34" Scale9OriginY="-45" Scale9Width="121" Scale9Height="160" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/black_platform.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="platform5" ActionTag="-446243980" VisibleForFrame="False" Tag="3112" IconVisible="False" LeftMargin="-26.5000" RightMargin="-26.5000" TopMargin="-35.0000" BottomMargin="-35.0000" LeftEage="87" RightEage="87" TopEage="115" BottomEage="115" Scale9OriginX="-34" Scale9OriginY="-45" Scale9Width="121" Scale9Height="160" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/yellow_platform.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="platform6" ActionTag="836376673" Tag="3109" IconVisible="False" LeftMargin="-26.5000" RightMargin="-26.5000" TopMargin="-35.0000" BottomMargin="-35.0000" LeftEage="87" RightEage="87" TopEage="115" BottomEage="115" Scale9OriginX="-34" Scale9OriginY="-45" Scale9Width="121" Scale9Height="160" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/red_platform.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="platform7" ActionTag="-354906898" VisibleForFrame="False" Tag="212" IconVisible="False" LeftMargin="-26.5000" RightMargin="-26.5000" TopMargin="-35.0000" BottomMargin="-35.0000" LeftEage="17" RightEage="17" TopEage="23" BottomEage="23" Scale9OriginX="17" Scale9OriginY="23" Scale9Width="19" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/green_platform.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tower_slot" ActionTag="-1286927179" Tag="8" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="cannon1" ActionTag="784491688" Tag="3102" IconVisible="False" LeftMargin="-12.9072" RightMargin="-35.0928" TopMargin="-13.6404" BottomMargin="-13.3596" LeftEage="15" RightEage="15" TopEage="8" BottomEage="8" Scale9OriginX="15" Scale9OriginY="8" Scale9Width="18" Scale9Height="11" ctype="ImageViewObjectData">
                <Size X="48.0000" Y="27.0000" />
                <Children>
                  <AbstractNodeData Name="bullet_spawn1" ActionTag="156802641" Tag="242" IconVisible="True" LeftMargin="57.0902" RightMargin="-9.0902" TopMargin="13.2168" BottomMargin="13.7832" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="57.0902" Y="13.7832" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1894" Y="0.5105" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.2689" ScaleY="0.4948" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/single_cannon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="cannon2" ActionTag="-2015942171" VisibleForFrame="False" Tag="3103" IconVisible="False" LeftMargin="-14.5000" RightMargin="-14.5000" TopMargin="-22.0000" BottomMargin="-22.0000" LeftEage="9" RightEage="9" TopEage="14" BottomEage="14" Scale9OriginX="9" Scale9OriginY="14" Scale9Width="11" Scale9Height="16" ctype="ImageViewObjectData">
                <Size X="29.0000" Y="44.0000" />
                <Children>
                  <AbstractNodeData Name="bullet_spawn1" ActionTag="1560300456" Tag="243" IconVisible="True" LeftMargin="41.1091" RightMargin="-12.1091" TopMargin="7.6268" BottomMargin="36.3732" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="41.1091" Y="36.3732" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.4176" Y="0.8267" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bullet_spawn2" ActionTag="-1541772357" Tag="3108" IconVisible="True" LeftMargin="41.9000" RightMargin="-12.9000" TopMargin="35.5914" BottomMargin="8.4086" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="41.9000" Y="8.4086" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.4448" Y="0.1911" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/double_cannon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="cannon3" ActionTag="1341648637" VisibleForFrame="False" Tag="3104" IconVisible="False" LeftMargin="-22.5000" RightMargin="-22.5000" TopMargin="-22.5000" BottomMargin="-22.5000" LeftEage="14" RightEage="14" TopEage="14" BottomEage="14" Scale9OriginX="14" Scale9OriginY="14" Scale9Width="17" Scale9Height="17" ctype="ImageViewObjectData">
                <Size X="45.0000" Y="45.0000" />
                <Children>
                  <AbstractNodeData Name="bullet_spawn1" ActionTag="1518286220" Tag="378" IconVisible="True" LeftMargin="75.3425" RightMargin="-30.3425" TopMargin="21.8527" BottomMargin="23.1473" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="75.3425" Y="23.1473" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.6743" Y="0.5144" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bullet_spawn2" ActionTag="1821260795" Tag="3105" IconVisible="True" LeftMargin="22.9588" RightMargin="22.0412" TopMargin="-43.0974" BottomMargin="88.0974" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="22.9588" Y="88.0974" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5102" Y="1.9577" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bullet_spawn3" ActionTag="-704214848" Tag="3106" IconVisible="True" LeftMargin="-35.9098" RightMargin="80.9098" TopMargin="22.4371" BottomMargin="22.5629" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="-35.9098" Y="22.5629" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.7980" Y="0.5014" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bullet_spawn4" ActionTag="860846115" Tag="3107" IconVisible="True" LeftMargin="21.8709" RightMargin="23.1291" TopMargin="87.2657" BottomMargin="-42.2657" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="21.8709" Y="-42.2657" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4860" Y="-0.9392" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Textures/cross_cannon.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="shield" ActionTag="-278340393" VisibleForFrame="False" Alpha="151" Tag="344" IconVisible="False" LeftMargin="-50.7102" RightMargin="-53.2898" TopMargin="-50.1761" BottomMargin="-51.8239" LeftEage="42" RightEage="42" TopEage="41" BottomEage="41" Scale9OriginX="42" Scale9OriginY="41" Scale9Width="20" Scale9Height="20" ctype="ImageViewObjectData">
            <Size X="104.0000" Y="102.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.2898" Y="-0.8239" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Textures/shield.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>
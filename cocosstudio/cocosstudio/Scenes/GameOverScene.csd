<GameFile>
  <PropertyGroup Name="GameOverScene" Type="Scene" ID="0daea76c-0098-4d0f-b989-34f76435ac1e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" CustomClassName="GameOverScene" Tag="257" ctype="GameNodeObjectData">
        <Size X="1024.0000" Y="768.0000" />
        <Children>
          <AbstractNodeData Name="player_score_label" ActionTag="509576484" Tag="261" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="281.8781" RightMargin="403.1218" TopMargin="398.5463" BottomMargin="333.4537" FontSize="36" LabelText="Yours score:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="339.0000" Y="36.0000" />
            <Children>
              <AbstractNodeData Name="value" ActionTag="-129061777" Tag="262" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="352.6005" RightMargin="-105.6005" TopMargin="-0.3687" BottomMargin="0.3687" FontSize="36" LabelText="999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="92.0000" Y="36.0000" />
                <AnchorPoint ScaleY="0.4898" />
                <Position X="352.6005" Y="18.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0401" Y="0.5000" />
                <PreSize X="0.2714" Y="1.0000" />
                <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="451.3781" Y="351.4537" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4408" Y="0.4576" />
            <PreSize X="0.3311" Y="0.0469" />
            <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="gameover_label" ActionTag="-818327465" Tag="259" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="326.0000" RightMargin="326.0000" TopMargin="204.4000" BottomMargin="511.6000" FontSize="48" LabelText="Game Over" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="372.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="512.0000" Y="537.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7000" />
            <PreSize X="0.3633" Y="0.0677" />
            <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="restart_btn" ActionTag="-359765424" Alpha="177" Tag="258" IconVisible="False" PercentHeightEnable="True" PercentHeightEnabled="True" LeftMargin="230.5221" RightMargin="216.4779" TopMargin="569.8983" BottomMargin="94.1017" TouchEnable="True" FontSize="40" ButtonText="Restart" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="547" Scale9Height="82" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="577.0000" Y="104.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="519.0221" Y="146.1017" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5069" Y="0.1902" />
            <PreSize X="0.5635" Y="0.1354" />
            <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Normal" Path="Textures/btn_pressed.png" Plist="" />
            <PressedFileData Type="Normal" Path="Textures/btn_pressed.png" Plist="" />
            <NormalFileData Type="Normal" Path="Textures/btn_normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>
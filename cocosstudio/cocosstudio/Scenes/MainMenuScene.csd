<GameFile>
  <PropertyGroup Name="MainMenuScene" Type="Scene" ID="069daa43-0695-4e7c-9769-fd7e4c2dfaf5" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" CustomClassName="MainMenuScene" Tag="3059" ctype="GameNodeObjectData">
        <Size X="1024.0000" Y="768.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="121944804" Tag="190" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="299" RightEage="299" TopEage="224" BottomEage="224" Scale9OriginX="299" Scale9OriginY="224" Scale9Width="310" Scale9Height="233" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="768.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="512.0000" Y="384.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Textures/background_grass.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_label_black" ActionTag="-1815813791" Tag="3060" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="228.7344" RightMargin="217.2656" TopMargin="138.2472" BottomMargin="398.7528" ctype="SpriteObjectData">
            <Size X="578.0000" Y="231.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="517.7344" Y="514.2528" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.5056" Y="0.6696" />
            <PreSize X="0.5645" Y="0.3008" />
            <FileData Type="Normal" Path="Textures/title_label.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_label" ActionTag="1025433326" Tag="3302" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="237.2336" RightMargin="208.7664" TopMargin="147.8472" BottomMargin="389.1528" ctype="SpriteObjectData">
            <Size X="578.0000" Y="231.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="526.2336" Y="504.6528" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5139" Y="0.6571" />
            <PreSize X="0.5645" Y="0.3008" />
            <FileData Type="Normal" Path="Textures/title_label.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="play_btn" ActionTag="1627610495" Alpha="177" Tag="191" IconVisible="False" LeftMargin="242.5818" RightMargin="204.4182" TopMargin="466.5269" BottomMargin="197.4731" TouchEnable="True" FontSize="40" ButtonText="Play" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="547" Scale9Height="82" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="577.0000" Y="104.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="531.0818" Y="249.4731" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5186" Y="0.3248" />
            <PreSize X="0.5635" Y="0.1354" />
            <FontResource Type="Normal" Path="Fonts/Roundrick_bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Normal" Path="Textures/btn_pressed.png" Plist="" />
            <PressedFileData Type="Normal" Path="Textures/btn_pressed.png" Plist="" />
            <NormalFileData Type="Normal" Path="Textures/btn_normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-1057751932" Tag="243" IconVisible="False" LeftMargin="27.4541" RightMargin="348.5459" TopMargin="27.0333" BottomMargin="688.9667" FontSize="48" LabelText="Мишаня, а где твои танки ????" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="648.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="351.4541" Y="714.9667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3432" Y="0.9309" />
            <PreSize X="0.6328" Y="0.0677" />
            <FontResource Type="Normal" Path="Fonts/calibrib.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>